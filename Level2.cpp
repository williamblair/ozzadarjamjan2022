#include <Level2.h>

#include <Renderer.h>
#include <GameTimer.h>
#include <Input.h>
#include <Shader.h>
#include <Player.h>
#include <SoundEffect.h>
#include <AssimpModel.h>
#include <Sprite.h>
#include <FreetypeFont.h>
#include <TextDialogue.h>

// Globals from main.cpp
extern int S_WIDTH;
extern int S_HEIGHT;
extern Renderer render;
extern GameTimer timer;
extern Input input;

static bool PlayerNextToDoor(Player& player, TileMap& map)
{
    AABB doorAabb;
    const float tileSize = 5.0f;
    const float tileOffs = tileSize / 2.0f;
    doorAabb.pos = map.GetTilePosition(8,4);
    doorAabb.min = Math::Vec3FromVals(-tileOffs, -tileOffs, -tileOffs);
    doorAabb.max = Math::Vec3FromVals(
        tileOffs + tileSize, // include the next tile to the right also
        tileOffs,
        tileOffs); 
    return player.GetAabb().Intersects(doorAabb);
}

static bool PlayerNextToTable(Player& player, AssimpModel& table)
{
    const float tileSize = 5.0f;
    const float tileOffs = tileSize / 2.0f;
    // AABB at the model position with the size of a full tile
    AABB modelAabb = table.GetCollider();
    modelAabb.min = Math::Vec3FromVals(-tileOffs, -tileOffs, -tileOffs);
    modelAabb.max = Math::Vec3FromVals(tileOffs, tileOffs, tileOffs);
    return player.GetAabb().Intersects(modelAabb);
}

static bool PlayerNextToMask(Player& player, AssimpModel& mask)
{
    const float tileSize = 5.0f;
    const float tileOffs = tileSize / 2.0f;
    // AABB at the model position with the size of a full tile
    AABB modelAabb = mask.GetCollider();
    modelAabb.min = Math::Vec3FromVals(-tileOffs, -tileOffs, -tileOffs);
    modelAabb.max = Math::Vec3FromVals(tileOffs, tileOffs, tileOffs);
    return player.GetAabb().Intersects(modelAabb);
}

inline bool MouseOverlaps11(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 200 &&
        input.MouseX() <= (200 + strlen("11")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 100) &&
        input.MouseY() >= ((S_HEIGHT - 100) - fontHeight));
}

inline bool MouseOverlaps16(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 575 &&
        input.MouseX() <= (575 + strlen("16")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 100) &&
        input.MouseY() >= ((S_HEIGHT - 100) - fontHeight));
}

inline bool MouseOverlaps23(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 200 &&
        input.MouseX() <= (200 + strlen("23")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 50) &&
        input.MouseY() >= ((S_HEIGHT - 50) - fontHeight));
}

inline bool MouseOverlaps28(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 500 &&
        input.MouseX() <= (575 + strlen("28")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 50) &&
        input.MouseY() >= ((S_HEIGHT - 50) - fontHeight));
}

static void DrawQuestionChoices(
    FreetypeFont& font,
    FreetypeFont& fontColored)
{
    FreetypeFont* f = MouseOverlaps11(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "11", 200, S_HEIGHT - 100);
    
    f = MouseOverlaps16(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "16", 575, S_HEIGHT - 100);
    
    f = MouseOverlaps23(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "23", 200, S_HEIGHT - 50);
    
    f = MouseOverlaps28(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "28", 575, S_HEIGHT - 50);
}

bool Level2()
{
    AssimpModel tableModel;
    AssimpModel maskModel;
    TileMap map;
    Shader tileMapShader;
    Shader* modelShader;
    Sprite dialogueBg;
    Sprite info1Spr;
    Sprite info2Spr;
    Sprite* curInfoSpr = nullptr;
    Sprite scalpelSprite;
    Player player;
    SoundEffect walkEffect;
    SoundEffect lockedEffect; // door locked
    SoundEffect openEffect; // door opened
    SoundEffect unlockedEffect; // door unlocked
    SoundEffect paperFlipEffect;
    FreetypeFont dialogueFont;
    FreetypeFont dialogueFontColored;
    TextDialogue textDialogue;
    bool runningTextDialogue = false;
    bool showingInfoText = false;
    
    int itemCount = 0;
    const int requiredItems = 2;
    bool tableCollected = false;
    bool maskCollected = false;
    
    tableModel.Load("assets/Autopsy_Table_bj.obj");
    {
        const float tileSize = 5.0f;
        Vec3 pos = map.GetTilePosition(6, 2);
        pos.y -= tileSize / 2.0f;
        tableModel.SetPosition(pos);
        tableModel.SetScale(Vec3(0.30f, 0.30f, 0.30f));
    }
    maskModel.Load("assets/gasmask_bj.obj");
    {
        const float tileSize = 5.0f;
        Vec3 pos = map.GetTilePosition(5, 6);
        pos.y -= tileSize / 2.0f;
        maskModel.SetPosition(pos);
        maskModel.SetScale(Vec3(1.0f, 1.0f, 1.0f));
    }
    
    tileMapShader.Init("Shaders/TexturedVert.glsl", "Shaders/TexturedFrag.glsl");
    modelShader = &tileMapShader; // both used texturedvert/frag
    
    map.Load("assets/level2.txt");
    map.AddTexture(0, "assets/greentile.jpg");
    map.AddTexture(1, "assets/greenwallbj.jpg");
    map.AddTexture(2, "assets/greendoorleft.jpg");
    map.AddTexture(3, "assets/greendoorright.jpg");
    map.AddCeilTexture("assets/greenwallbj.jpg");
    
    dialogueBg.Load("assets/dialoguebg.jpg", S_WIDTH, S_HEIGHT);
    info1Spr.Load("assets/sizemore1.png", S_WIDTH, S_HEIGHT);
    info2Spr.Load("assets/sizemore2.png", S_WIDTH, S_HEIGHT);
    scalpelSprite.Load("assets/scalpel.png", S_WIDTH, S_HEIGHT);
    
    walkEffect.Load("assets/jute-dh-steps/stepstone_1.wav");
    lockedEffect.Load("assets/doorlocked.wav");
    openEffect.Load("assets/dooropen.ogg");
    unlockedEffect.Load("assets/doorunlock.mp3");
    paperFlipEffect.Load("assets/paperflipeffect.mp3");
    
    dialogueFont.Load(
        "assets/Alice-Regular.ttf",
        S_WIDTH, S_HEIGHT,
        255,255,255,
        18
    );
    dialogueFontColored.Load(
        "assets/Alice-Regular.ttf",
        S_WIDTH, S_HEIGHT,
        255,255,0,
        18
    );
    
    player.SetWalkEffect(&walkEffect);
    {
        const float tileSize = 5.0f;
        Vec3 tileCenter = map.GetTilePosition(1, 1);
        tileCenter.y -= tileSize / 2.0f;
        player.SetPosition(tileCenter);
    }
    player.SetYaw(270.0f);
    
    player.AddColliders(map.GetColliders());
    player.AddCollider(tableModel.GetCollider());
    player.AddCollider(maskModel.GetCollider());
    
    bool quit = false;
    while (!quit && !input.Quit())
    {
        const float dt = timer.Update();
        render.Clear();
        
        // LOGIC -------------------------------------------------
        bool playerNextToDoor = false;
        bool playerNextToTable = false;
        bool playerNextToMask = false;
        if (!runningTextDialogue && !showingInfoText) {
            playerNextToDoor = PlayerNextToDoor(player, map);
            if (!tableCollected) {
                playerNextToTable = PlayerNextToTable(player, tableModel);
            }
            if (!maskCollected) {
                playerNextToMask = PlayerNextToMask(player, maskModel);
            }
            player.Move(input, dt);
            
            if (playerNextToDoor) {
                if (input.MouseClicked()) {
                    if (itemCount == requiredItems) {
                        openEffect.Play();
                        quit = true;
                    }
                    else {
                        lockedEffect.Play();
                        runningTextDialogue = true;
                        textDialogue.SetText("The door seems to be locked.");
                        textDialogue.SetTextSpeed(0.025f);
                        textDialogue.SetX(150+10);
                        textDialogue.SetY(S_HEIGHT - dialogueBg.GetHeight() + 50);
                        textDialogue.SetFont(&dialogueFont);
                    }
                }
            }
            
            else if (playerNextToTable) {
                if (input.MouseClicked()) {
                    paperFlipEffect.Play();
                    if (!tableCollected) {
                        tableCollected = true;
                        ++itemCount;
                        player.RemoveCollider(tableModel.GetCollider());
                        showingInfoText = true;
                        if (itemCount == 1) { curInfoSpr = &info1Spr; }
                        else                { curInfoSpr = &info2Spr; }
                    }
                }
            }
            else if (playerNextToMask) {
                if (input.MouseClicked()) {
                    paperFlipEffect.Play();
                    if (!maskCollected) {
                        maskCollected = true;
                        ++itemCount;
                        player.RemoveCollider(maskModel.GetCollider());
                        showingInfoText = true;
                        if (itemCount == 1) { curInfoSpr = &info1Spr; }
                        else                { curInfoSpr = &info2Spr; }
                    }
                }
            }
        }
        else if (runningTextDialogue) {
            player.StopWalkEffect();
            textDialogue.Update(dt);
            if (textDialogue.Done() &&
                input.MouseClicked())
            {
                runningTextDialogue = false;
            }
        }
        // showingInfoText == true
        else {
            player.StopWalkEffect();
            if (input.MouseClicked()) {
                showingInfoText = false;
                if (itemCount == 2) { unlockedEffect.Play(); }
            }
        }
        
        // DRAW --------------------------------------------------
        tileMapShader.Use();
        map.Draw(render, player.GetViewMat(), tileMapShader);
        
        if (!tableCollected) {
            modelShader->Use();
            tableModel.Draw(render, player.GetViewMat(), *modelShader);
        }
        if (!maskCollected) {
            modelShader->Use();
            maskModel.Draw(render, player.GetViewMat(), *modelShader);
        }
        
        if (runningTextDialogue) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            textDialogue.Draw(render);
        }
        else if (showingInfoText) {
            curInfoSpr->Draw(
                render,
                50, 50
            );
        }
        else if (playerNextToDoor) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            dialogueFont.PrintString(
                render,
                "Mouse Click: Open",
                150+10, S_HEIGHT - dialogueBg.GetHeight() + 50
            );
        }
        else if (playerNextToTable || playerNextToMask) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            dialogueFont.PrintString(
                render,
                "Mouse Click: Inspect",
                150+10, S_HEIGHT - dialogueBg.GetHeight() + 50
            );
        }
        
        player.Update();
        input.Update();
        render.Update();
    }
    
    player.StopWalkEffect();
    
    quit = false;
    bool passed = false;
    runningTextDialogue = true;
    textDialogue.SetText("How many minutes was Sizemore awake?");
    textDialogue.SetTextSpeed(0.025f);
    textDialogue.SetX(150+10);
    textDialogue.SetY(S_HEIGHT - dialogueBg.GetHeight() + 50);
    textDialogue.SetFont(&dialogueFont);
    while (!input.Quit() && !quit)
    {
        float dt = timer.Update();
        render.Clear();
        
        // LOGIC --------------------------------------------------
        if (runningTextDialogue) {
            textDialogue.Update(dt);
            if (textDialogue.Done() &&
                input.MouseClicked())
            {
                runningTextDialogue = false;
            }
        }
        // User input select
        else {
            if (input.MouseClicked()) {
                if (MouseOverlaps11(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    std::cout << "Mouse overlaps 11, passed = false" << std::endl;
                    passed = false;
                    quit = true;
                }
                else if (MouseOverlaps16(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    std::cout << "Mouse overlaps 16, correct, passed" << std::endl;
                    passed = true;
                    quit = true;
                }
                else if (MouseOverlaps23(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    std::cout << "Mouse overlaps 23, passed = false" << std::endl;
                    passed = false;
                    quit = true;
                }
                else if (MouseOverlaps28(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    std::cout << "Mouse overlaps 28, passed = false" << std::endl;
                    passed = false;
                    quit = true;
                }
            }
        }
        
        // DRAW ---------------------------------------------------
        tileMapShader.Use();
        map.Draw(render, player.GetViewMat(), tileMapShader);
        if (runningTextDialogue) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            textDialogue.Draw(render);
        }
        // user question guess input
        else {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            DrawQuestionChoices(dialogueFont, dialogueFontColored);
            scalpelSprite.Draw(render, input.MouseX(), input.MouseY());
        }
        
        player.Update();
        input.Update();
        render.Update();
    }
    
    
    return passed;
}

