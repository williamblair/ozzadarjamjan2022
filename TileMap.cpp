#include <TileMap.h>

#include <sstream>
#include <fstream>

TileMap::TileMap() :
    mRows(0),
    mCols(0),
    mTileSize(5),
    mPosition(Vec3(0.0f, 0.0f, 0.0f))
{}

TileMap::~TileMap()
{}

void TileMap::Load(const std::string& fileName)
{
    std::ifstream mapFile(fileName);
    if (!mapFile.is_open()) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "Failed to open map: " << fileName << std::endl;
        throw std::runtime_error("Failed to open tile map");
    }
    
    std::stringstream mapFileStream;
    mapFileStream << mapFile.rdbuf();
    
    mapFileStream >> mRows >> mCols;
    
    mMap.resize(mRows);
    for (auto& row : mMap) {
        row.resize(mCols);
    }
    for (int row = 0; row < mRows; ++row)
    {
        for (int col = 0; col < mCols; ++col)
        {
            if (!mapFileStream) {
                throw std::runtime_error("Bad TileMap filestream");
            }
            
            int val;
            mapFileStream >> val;
            mMap[row][col] = val;
        }
    }
    
    // DEBUG PRINT
    #if 1
    std::cout << "Tile map:" << std::endl;
    for (int row = 0; row < mRows; ++row)
    {
        for (int col = 0; col < mCols; ++col)
        {
            std::cout << mMap[row][col];
        }
        std::cout << std::endl;
    }
    #endif
    
    InitVertBuff();
    InitAabbs();
}

void TileMap::AddTexture(const int mapVal, const std::string& fileName)
{
    mTextures.push_back(std::make_shared<Texture>());
    auto tex = *mTextures.rbegin();
    if (!tex->Load(fileName)) {
        throw std::runtime_error("TileMap failed to load texture");
    }
    
    mTexMap[mapVal] = mTextures.size()-1;
}

void TileMap::AddCeilTexture(const std::string& fileName)
{
    mCeilTexture.Load(fileName);
}

void TileMap::InitVertBuff()
{
    const float f = float(mTileSize) / 2.0f;
    std::vector<float> cubeVerts = {
        // position texcoord
        
        // front
        -f, -f, f,  0.0f, 0.0f,
        f, -f, f,   1.0f, 0.0f,
        f, f, f,    1.0f, 1.0f,
        -f, -f, f,  0.0f, 0.0f,
        f, f, f,    1.0f, 1.0f,
        -f, f, f,   0.0f, 1.0f,
        
        // back
        f, -f, -f,  0.0f, 0.0f,
        -f, -f, -f, 1.0f, 0.0f,
        -f, f, -f,  1.0f, 1.0f,
        f, -f, -f,  0.0f, 0.0f,
        -f, f, -f,  1.0f, 1.0f,
        f, f, -f,   0.0f, 1.0f,
        
        // left
        -f, -f, -f, 0.0f, 0.0f,
        -f, -f, f,  1.0f, 0.0f,
        -f, f, f,   1.0f, 1.0f,
        -f, -f, -f, 0.0f, 0.0f,
        -f, f, f,   1.0f, 1.0f,
        -f, f, -f,  0.0f, 1.0f,
        
        // right
        f, -f, f,   0.0f, 0.0f,
        f, -f, -f,  1.0f, 0.0f,
        f, f, -f,   1.0f, 1.0f,
        f, -f, f,   0.0f, 0.0f,
        f, f, -f,   1.0f, 1.0f,
        f, f, f,    0.0f, 1.0f,
        
        // top
        -f, f, f,   0.0f, 0.0f,
        f, f, f,    1.0f, 0.0f,
        f, f, -f,   1.0f, 1.0f,
        -f, f, f,   0.0f, 0.0f,
        f, f, -f,   1.0f, 1.0f,
        -f, f, -f,  0.0f, 1.0f,
        
        // bottom
        -f, -f, -f, 0.0f, 0.0f,
        f, -f, -f,  1.0f, 0.0f,
        f, -f, f,   1.0f, 1.0f,
        -f, -f, -f, 0.0f, 0.0f,
        f, -f, f,   1.0f, 1.0f,
        -f, -f, f,  0.0f, 1.0f
    };
    
    // 5 floats per vertex
    mVertBuf.Init(cubeVerts, 5);
}

void TileMap::InitAabbs()
{
    const float f = float(mTileSize) / 2.0f;
    Mat4 mapModelMat = Math::translate(mPosition);
    for (int row = 0; row < mRows; ++row)
    {
        float tileOffsX = 0.0f;
        float tileOffsZ = row * mTileSize;
        for (int col = 0; col < mCols; ++col)
        {
            int mapVal = mMap[row][col];
            // 0 = ground, all others are wall
            if (mapVal >= 1)
            {
                AABB newAabb;
                mWallAabbs.push_back(newAabb);
                AABB& aabb = *mWallAabbs.rbegin();
                
                aabb.min = Math::Vec3FromVals(-f, -f, -f);
                aabb.max = Math::Vec3FromVals(f, f, f);
                Vec3 pos = mPosition;
                // TODO - not hardcode wall y positions
                pos = Math::Transform(
                    Math::translate(tileOffsX, 0.0f, tileOffsZ),
                    pos
                );
                aabb.pos = pos;
            }
            
            tileOffsX += mTileSize;
        }
    }
}

void TileMap::Draw(Renderer& render, Mat4& viewMat, Shader& shader)
{
    Mat4 mapModelMat = Math::translate(mPosition);
    Mat4 mvpMat =
        render.GetPerspMat() *
        viewMat *
        mapModelMat;
    for (int row = 0; row < mRows; ++row)
    {
        float tileOffsX = 0.0f;
        float tileOffsZ = row * mTileSize;
        for (int col = 0; col < mCols; ++col)
        {
            int mapVal = mMap[row][col];
            //std::cout << "Drawing map val: " << mapVal << std::endl;
            
            if (mTexMap.find(mapVal) == mTexMap.end()) {
                throw std::runtime_error("Failed to find texture for map val");
            }
            auto tex = mTextures[mTexMap[mapVal]];
            //std::cout << "Tex ptr: " << tex << std::endl;
            if (tex == nullptr) {
                throw std::runtime_error("Texture is null");
            }
            float tileOffsY = 0.0f;
            
            switch (mapVal)
            {
            // 0 reserved for ground
            case 0: tileOffsY = -mTileSize; break;
            // all others are wall types
            case 1:
            case 2:
            case 3:
                tileOffsY = 0.0f; break;
            default:
                throw std::runtime_error("Unhandled map value");
                break;
            }
            
            Vec3 tileOffs(tileOffsX, tileOffsY, tileOffsZ);
            Mat4 tileModelMat = Math::translate(tileOffs);
            
            shader.SetMat4(
                "uTransform",
                mvpMat * 
                tileModelMat
            );
            render.SetTexture(*tex);
            render.DrawVertexBuffer(mVertBuf);
            
            tileOffsX += mTileSize;
        }
    }
    
    // Draw ceiling
    render.SetTexture(mCeilTexture);
    for (int row = 0; row < mRows; ++row)
    {
        float tileOffsX = 0.0f;
        float tileOffsZ = row * mTileSize;
        for (int col = 0; col < mCols; ++col)
        {
            float tileOffsY = mTileSize;
            
            Vec3 tileOffs(tileOffsX, tileOffsY, tileOffsZ);
            Mat4 tileModelMat = Math::translate(tileOffs);
            
            shader.SetMat4(
                "uTransform",
                mvpMat * 
                tileModelMat
            );
            render.DrawVertexBuffer(mVertBuf);
            
            tileOffsX += mTileSize;
        }
    }
}

