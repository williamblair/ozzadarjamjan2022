#include <Level4.h>

#include <Renderer.h>
#include <GameTimer.h>
#include <Input.h>
#include <Shader.h>
#include <Player.h>
#include <SoundEffect.h>
#include <AssimpModel.h>
#include <Sprite.h>
#include <FreetypeFont.h>
#include <TextDialogue.h>

// Globals from main.cpp
extern int S_WIDTH;
extern int S_HEIGHT;
extern Renderer render;
extern GameTimer timer;
extern Input input;

static bool PlayerNextToDoor(Player& player, TileMap& map)
{
    AABB doorAabb;
    const float tileSize = 5.0f;
    const float tileOffs = tileSize / 2.0f;
    doorAabb.pos = map.GetTilePosition(1,4);
    doorAabb.min = Math::Vec3FromVals(-tileOffs, -tileOffs, -tileOffs);
    doorAabb.max = Math::Vec3FromVals(
        tileOffs + tileSize, // include the next tile to the right also
        tileOffs,
        tileOffs);
    return player.GetAabb().Intersects(doorAabb);
}

static bool PlayerNextToAirplane(Player& player, AssimpModel& airplane)
{
    const float tileSize = 5.0f;
    const float tileOffs = tileSize / 2.0f;
    // AABB at the model position with the size of a full tile
    AABB modelAabb = airplane.GetCollider();
    modelAabb.min = Math::Vec3FromVals(-tileOffs, -tileOffs, -tileOffs);
    modelAabb.max = Math::Vec3FromVals(tileOffs, tileOffs, tileOffs);
    return player.GetAabb().Intersects(modelAabb);
}

static bool PlayerNextToMan(Player& player, AssimpModel& man)
{
    const float tileSize = 5.0f;
    const float tileOffs = tileSize / 2.0f;
    // AABB at the model position with the size of a full tile
    AABB modelAabb = man.GetCollider();
    modelAabb.min = Math::Vec3FromVals(-tileOffs, -tileOffs, -tileOffs);
    modelAabb.max = Math::Vec3FromVals(tileOffs, tileOffs, tileOffs);
    return player.GetAabb().Intersects(modelAabb);
}

static bool MouseOverlaps10(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 200 &&
        input.MouseX() <= (200 + strlen("10")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 100) &&
        input.MouseY() >= ((S_HEIGHT - 100) - fontHeight));
}

static bool MouseOverlaps11(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 575 &&
        input.MouseX() <= (575 + strlen("11")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 100) &&
        input.MouseY() >= ((S_HEIGHT - 100) - fontHeight));
}

static bool MouseOverlaps12(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 200 &&
        input.MouseX() <= (200 + strlen("12")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 50) &&
        input.MouseY() >= ((S_HEIGHT - 50) - fontHeight));
}

static bool MouseOverlaps13(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 575 &&
        input.MouseX() <= (575 + strlen("13")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 50) &&
        input.MouseY() >= ((S_HEIGHT - 50) - fontHeight));
}

static void DrawQuestionChoices(
    FreetypeFont& font,
    FreetypeFont& fontColored)
{
    FreetypeFont* f = MouseOverlaps10(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "10", 200, S_HEIGHT - 100);
    
    f = MouseOverlaps11(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "11", 575, S_HEIGHT - 100);
    
    f = MouseOverlaps12(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "12", 200, S_HEIGHT - 50);
    
    f = MouseOverlaps13(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "13", 575, S_HEIGHT - 50);
}

bool Level4()
{
    AssimpModel manModel;
    AssimpModel airplaneModel;
    Sprite dialogueBg;
    Sprite info1Spr;
    Sprite info2Spr;
    Sprite* curInfoSpr = nullptr;
    Sprite scalpelSprite;
    TileMap map;
    Shader tileMapShader;
    Shader* modelShader;
    Player player;
    SoundEffect walkEffect;
    SoundEffect lockedEffect; // door locked
    SoundEffect openEffect; // door opened
    SoundEffect unlockedEffect; // door unlocked
    SoundEffect paperFlipEffect;
    FreetypeFont dialogueFont;
    FreetypeFont dialogueFontColored;
    TextDialogue textDialogue;
    bool runningTextDialogue = false;
    bool showingInfoText = false;
    
    int itemCount = 0;
    const int requiredItems = 2;
    bool airplaneCollected = false;
    bool manCollected = false;
    
    manModel.Load("assets/mannequinbj.obj");
    {
        const float tileSize = 5.0f;
        Vec3 pos = map.GetTilePosition(8, 1);
        pos.y -= tileSize / 2.0f;
        manModel.SetPosition(pos);
        manModel.SetScale(Vec3(3.0f, 3.0f, 3.0f));
    }
    airplaneModel.Load("assets/airplanebj.obj"); // TODO - textures
    {
        const float tileSize = 5.0f;
        Vec3 pos = map.GetTilePosition(8, 8);
        pos.y -= tileSize / 2.0f;
        airplaneModel.SetPosition(pos);
        airplaneModel.SetScale(Vec3(0.5f, 0.5f, 0.5f));
    }
    
    tileMapShader.Init("Shaders/TexturedVert.glsl", "Shaders/TexturedFrag.glsl");
    modelShader = &tileMapShader; // both used texturedvert/frag
    
    map.Load("assets/level4.txt");
    map.AddTexture(0, "assets/blackrough.jpg");
    map.AddTexture(1, "assets/mold.jpg");
    map.AddTexture(2, "assets/blackdoorleft.jpg");
    map.AddTexture(3, "assets/blackdoorright.jpg");
    map.AddCeilTexture("assets/blackbrick.jpg");
    
    dialogueBg.Load("assets/dialoguebg.jpg", S_WIDTH, S_HEIGHT);
    info1Spr.Load("assets/sharilene1.jpg", S_WIDTH, S_HEIGHT);
    info2Spr.Load("assets/sharilene2.jpg", S_WIDTH, S_HEIGHT);
    scalpelSprite.Load("assets/scalpel.png", S_WIDTH, S_HEIGHT);
    
    walkEffect.Load("assets/jute-dh-steps/stepstone_1.wav");
    lockedEffect.Load("assets/doorlocked.wav");
    openEffect.Load("assets/dooropen.ogg");
    unlockedEffect.Load("assets/doorunlock.mp3");
    paperFlipEffect.Load("assets/paperflipeffect.mp3");
    
    dialogueFont.Load(
        "assets/Alice-Regular.ttf",
        S_WIDTH, S_HEIGHT,
        255,255,255,
        18
    );
    dialogueFontColored.Load(
        "assets/Alice-Regular.ttf",
        S_WIDTH, S_HEIGHT,
        255,255,0,
        18
    );
    
    player.SetWalkEffect(&walkEffect);
    {
        const float tileSize = 5.0f;
        Vec3 tileCenter = map.GetTilePosition(1, 1);
        tileCenter.y -= tileSize / 2.0f;
        player.SetPosition(tileCenter);
    }
    player.SetYaw(180.0f);
    
    player.AddColliders(map.GetColliders());
    player.AddCollider(airplaneModel.GetCollider());
    player.AddCollider(manModel.GetCollider());
    
    bool quit = false;
    while (!quit && !input.Quit())
    {
        float dt = timer.Update();
        
        render.Clear();
        
        // LOGIC -----------------------------------------------------
        bool playerNextToDoor = false;
        bool playerNextToAirplane = false;
        bool playerNextToMan = false;
        if (!runningTextDialogue && !showingInfoText) {
            playerNextToDoor = PlayerNextToDoor(player, map);
            if (!airplaneCollected) {
                playerNextToAirplane = PlayerNextToAirplane(player, airplaneModel);
            }
            if (!manCollected) {
                playerNextToMan = PlayerNextToMan(player, manModel);
            }
            player.Move(input, dt);
            
            if (playerNextToDoor) {
                if (input.MouseClicked()) {
                    if (itemCount == requiredItems) {
                        openEffect.Play();
                        quit = true;
                    }
                    else {
                        lockedEffect.Play();
                        runningTextDialogue = true;
                        textDialogue.SetText("The door seems to be locked.");
                        textDialogue.SetTextSpeed(0.025f);
                        textDialogue.SetX(150+10);
                        textDialogue.SetY(S_HEIGHT - dialogueBg.GetHeight() + 50);
                        textDialogue.SetFont(&dialogueFont);
                    }
                }
            }
            
            else if (playerNextToAirplane) {
                if (input.MouseClicked()) {
                    paperFlipEffect.Play();
                    if (!airplaneCollected) {
                        airplaneCollected = true;
                        ++itemCount;
                        player.RemoveCollider(airplaneModel.GetCollider());
                        showingInfoText = true;
                        if (itemCount == 1) { curInfoSpr = &info1Spr; }
                        else                { curInfoSpr = &info2Spr; }
                    }
                }
            }
            else if (playerNextToMan) {
                if (input.MouseClicked()) {
                    paperFlipEffect.Play();
                    if (!manCollected) {
                        manCollected = true;
                        ++itemCount;
                        player.RemoveCollider(manModel.GetCollider());
                        showingInfoText = true;
                        if (itemCount == 1) { curInfoSpr = &info1Spr; }
                        else                { curInfoSpr = &info2Spr; }
                    }
                }
            }
        }
        else if (runningTextDialogue) {
            player.StopWalkEffect();
            textDialogue.Update(dt);
            if (textDialogue.Done() &&
                input.MouseClicked())
            {
                runningTextDialogue = false;
            }
        }
        // showingInfoText == true
        else {
            player.StopWalkEffect();
            if (input.MouseClicked()) {
                showingInfoText = false;
                if (itemCount == 2) { unlockedEffect.Play(); }
            }
        }
        
        // DRAWING ---------------------------------------------------
        tileMapShader.Use();
        map.Draw(render, player.GetViewMat(), tileMapShader);
        
        if (!airplaneCollected) {
            modelShader->Use();
            airplaneModel.Draw(render, player.GetViewMat(), *modelShader);
        }
        if (!manCollected) {
            modelShader->Use();
            manModel.Draw(render, player.GetViewMat(), *modelShader);
        }
        
        if (runningTextDialogue) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            textDialogue.Draw(render);
        }
        else if (showingInfoText) {
            curInfoSpr->Draw(
                render,
                50, 50
            );
        }
        else if (playerNextToDoor) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            dialogueFont.PrintString(
                render,
                "Mouse Click: Open",
                150+10, S_HEIGHT - dialogueBg.GetHeight() + 50
            );
        }
        else if (playerNextToAirplane || playerNextToMan) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            dialogueFont.PrintString(
                render,
                "Mouse Click: Inspect",
                150+10, S_HEIGHT - dialogueBg.GetHeight() + 50
            );
        }

        player.Update();
        input.Update();
        render.Update();
    }

    player.StopWalkEffect();
    
    if (input.Quit()) { return false; }
    
    quit = false;
    bool passed = false;
    runningTextDialogue = true;
    textDialogue.SetText("How many New Yorkers have died?");
    textDialogue.SetTextSpeed(0.025f);
    textDialogue.SetX(150+10);
    textDialogue.SetY(S_HEIGHT - dialogueBg.GetHeight() + 50);
    textDialogue.SetFont(&dialogueFont);
    while (!input.Quit() && !quit)
    {
        float dt = timer.Update();
        render.Clear();
        
        // LOGIC --------------------------------------------------
        if (runningTextDialogue) {
            textDialogue.Update(dt);
            if (textDialogue.Done() &&
                input.MouseClicked())
            {
                runningTextDialogue = false;
            }
        }
        // User input select
        else {
            if (input.MouseClicked()) {
                if (MouseOverlaps10(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    passed = false;
                    quit = true;
                }
                else if (MouseOverlaps11(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    passed = false;
                    quit = true;
                }
                else if (MouseOverlaps12(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    passed = true;
                    quit = true;
                }
                else if (MouseOverlaps13(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    passed = false;
                    quit = true;
                }
            }
        }
        
        // DRAW ---------------------------------------------------
        tileMapShader.Use();
        map.Draw(render, player.GetViewMat(), tileMapShader);
        if (runningTextDialogue) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            textDialogue.Draw(render);
        }
        // user question guess input
        else {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            DrawQuestionChoices(dialogueFont, dialogueFontColored);
            scalpelSprite.Draw(render, input.MouseX(), input.MouseY());
        }
        
        player.Update();
        input.Update();
        render.Update();
    }
    
    return passed;
}

