#include <Level3.h>

#include <Renderer.h>
#include <GameTimer.h>
#include <Input.h>
#include <Shader.h>
#include <Player.h>
#include <SoundEffect.h>
#include <AssimpModel.h>
#include <Sprite.h>
#include <FreetypeFont.h>
#include <TextDialogue.h>

// Globals from main.cpp
extern int S_WIDTH;
extern int S_HEIGHT;
extern Renderer render;
extern GameTimer timer;
extern Input input;

static bool PlayerNextToDoor(Player& player, TileMap& map)
{
    AABB doorAabb;
    const float tileSize = 5.0f;
    const float tileOffs = tileSize / 2.0f;
    doorAabb.pos = map.GetTilePosition(7,1);
    doorAabb.min = Math::Vec3FromVals(-tileOffs, -tileOffs, -tileOffs);
    doorAabb.max = Math::Vec3FromVals(
        tileOffs, 
        tileOffs,
        tileOffs + tileSize); // include the next tile to it also
    return player.GetAabb().Intersects(doorAabb);
}

static bool PlayerNextToBrain(Player& player, AssimpModel& brain)
{
    const float tileSize = 5.0f;
    const float tileOffs = tileSize / 2.0f;
    // AABB at the model position with the size of a full tile
    AABB modelAabb = brain.GetCollider();
    modelAabb.min = Math::Vec3FromVals(-tileOffs, -tileOffs, -tileOffs);
    modelAabb.max = Math::Vec3FromVals(tileOffs, tileOffs, tileOffs);
    return player.GetAabb().Intersects(modelAabb);
}

static bool PlayerNextToDrill(Player& player, AssimpModel& drill)
{
    const float tileSize = 5.0f;
    const float tileOffs = tileSize / 2.0f;
    // AABB at the model position with the size of a full tile
    AABB modelAabb = drill.GetCollider();
    modelAabb.min = Math::Vec3FromVals(-tileOffs, -tileOffs, -tileOffs);
    modelAabb.max = Math::Vec3FromVals(tileOffs, tileOffs, tileOffs);
    return player.GetAabb().Intersects(modelAabb);
}

inline bool MouseOverlapsNewJersey(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 200 &&
        input.MouseX() <= (200 + strlen("New Jersey")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 100) &&
        input.MouseY() >= ((S_HEIGHT - 100) - fontHeight));
}

inline bool MouseOverlapsConnecticut(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 450 &&
        input.MouseX() <= (450 + strlen("Connecticut")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 100) &&
        input.MouseY() >= ((S_HEIGHT - 100) - fontHeight));
}

inline bool MouseOverlapsMassachusets(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 200 &&
        input.MouseX() <= (200 + strlen("Massachusets")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 50) &&
        input.MouseY() >= ((S_HEIGHT - 50) - fontHeight));
}

inline bool MouseOverlapsRhodeIsland(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 450 &&
        input.MouseX() <= (450 + strlen("Rhode Island")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 50) &&
        input.MouseY() >= ((S_HEIGHT - 50) - fontHeight));
}

static void DrawQuestionChoices(
    FreetypeFont& font,
    FreetypeFont& fontColored)
{
    FreetypeFont* f = MouseOverlapsNewJersey(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "New Jersey", 200, S_HEIGHT - 100);
    
    f = MouseOverlapsConnecticut(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "Connecticut", 450, S_HEIGHT - 100);
    
    f = MouseOverlapsMassachusets(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "Massachusets", 200, S_HEIGHT - 50);
    
    f = MouseOverlapsRhodeIsland(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "Rhode Island", 450, S_HEIGHT - 50);
}

bool Level3()
{
    AssimpModel brainModel;
    AssimpModel drillModel;
    Texture brainTexture;
    TileMap map;
    Shader tileMapShader;
    Shader* modelShader;
    Sprite dialogueBg;
    Sprite info1Spr;
    Sprite info2Spr;
    Sprite* curInfoSpr = nullptr;
    Sprite scalpelSprite;
    Player player;
    SoundEffect walkEffect;
    SoundEffect lockedEffect; // door locked
    SoundEffect openEffect; // door opened
    SoundEffect unlockedEffect; // door unlocked
    SoundEffect paperFlipEffect;
    FreetypeFont dialogueFont;
    FreetypeFont dialogueFontColored;
    TextDialogue textDialogue;
    bool runningTextDialogue = false;
    bool showingInfoText = false;
    
    int itemCount = 0;
    const int requiredItems = 2;
    bool brainCollected = false;
    bool drillCollected = false;
    
    brainModel.Load("assets/Brain_bj.obj");
    {
        const float tileSize = 5.0f;
        Vec3 pos = map.GetTilePosition(2, 8);
        pos.y -= tileSize / 2.0f;
        brainModel.SetPosition(pos);
        brainModel.SetScale(Vec3(0.30f, 0.30f, 0.30f));
    }
    brainTexture.Load("assets/Brain.png");
    brainModel.SetTexture(&brainTexture);
    drillModel.Load("assets/Drill_bj.obj");
    {
        const float tileSize = 5.0f;
        Vec3 pos = map.GetTilePosition(3, 1);
        pos.y -= tileSize / 2.0f;
        drillModel.SetPosition(pos);
        drillModel.SetScale(Vec3(1.0f, 1.0f, 1.0f));
    }
    
    tileMapShader.Init("Shaders/TexturedVert.glsl", "Shaders/TexturedFrag.glsl");
    modelShader = &tileMapShader; // both used texturedvert/frag
    
    map.Load("assets/level3.txt");
    map.AddTexture(0, "assets/tilefloor.jpg");
    map.AddTexture(1, "assets/paddedwall.jpg");
    map.AddTexture(2, "assets/whitedoorleft.jpg");
    map.AddTexture(3, "assets/whitedoorright.jpg");
    map.AddCeilTexture("assets/paddedwall.jpg");
    
    dialogueBg.Load("assets/dialoguebg.jpg", S_WIDTH, S_HEIGHT);
    info1Spr.Load("assets/wrongsurgery1.png", S_WIDTH, S_HEIGHT);
    info2Spr.Load("assets/wrongsurgery2.png", S_WIDTH, S_HEIGHT);
    scalpelSprite.Load("assets/scalpel.png", S_WIDTH, S_HEIGHT);
    
    walkEffect.Load("assets/jute-dh-steps/stepstone_1.wav");
    lockedEffect.Load("assets/doorlocked.wav");
    openEffect.Load("assets/dooropen.ogg");
    unlockedEffect.Load("assets/doorunlock.mp3");
    paperFlipEffect.Load("assets/paperflipeffect.mp3");
    
    dialogueFont.Load(
        "assets/Alice-Regular.ttf",
        S_WIDTH, S_HEIGHT,
        255,255,255,
        18
    );
    dialogueFontColored.Load(
        "assets/Alice-Regular.ttf",
        S_WIDTH, S_HEIGHT,
        255,255,0,
        18
    );
    
    player.SetWalkEffect(&walkEffect);
    {
        const float tileSize = 5.0f;
        Vec3 tileCenter = map.GetTilePosition(1, 1);
        tileCenter.y -= tileSize / 2.0f;
        player.SetPosition(tileCenter);
    }
    player.SetYaw(270.0f);
    
    player.AddColliders(map.GetColliders());
    player.AddCollider(brainModel.GetCollider());
    player.AddCollider(drillModel.GetCollider());
    
    bool quit = false;
    while (!quit && !input.Quit())
    {
        const float dt = timer.Update();
        render.Clear();
        
        // LOGIC -------------------------------------------------
        bool playerNextToDoor = false;
        bool playerNextToBrain = false;
        bool playerNextToDrill = false;
        if (!runningTextDialogue && !showingInfoText) {
            playerNextToDoor = PlayerNextToDoor(player, map);
            if (!brainCollected) {
                playerNextToBrain = PlayerNextToBrain(player, brainModel);
            }
            if (!drillCollected) {
                playerNextToDrill = PlayerNextToDrill(player, drillModel);
            }
            player.Move(input, dt);
            
            if (playerNextToDoor) {
                if (input.MouseClicked()) {
                    if (itemCount == requiredItems) {
                        openEffect.Play();
                        quit = true;
                    }
                    else {
                        lockedEffect.Play();
                        runningTextDialogue = true;
                        textDialogue.SetText("The door seems to be locked.");
                        textDialogue.SetTextSpeed(0.025f);
                        textDialogue.SetX(150+10);
                        textDialogue.SetY(S_HEIGHT - dialogueBg.GetHeight() + 50);
                        textDialogue.SetFont(&dialogueFont);
                    }
                }
            }
            
            else if (playerNextToBrain) {
                if (input.MouseClicked()) {
                    paperFlipEffect.Play();
                    if (!brainCollected) {
                        brainCollected = true;
                        ++itemCount;
                        player.RemoveCollider(brainModel.GetCollider());
                        showingInfoText = true;
                        if (itemCount == 1) { curInfoSpr = &info1Spr; }
                        else                { curInfoSpr = &info2Spr; }
                    }
                }
            }
            else if (playerNextToDrill) {
                if (input.MouseClicked()) {
                    paperFlipEffect.Play();
                    if (!drillCollected) {
                        drillCollected = true;
                        ++itemCount;
                        player.RemoveCollider(drillModel.GetCollider());
                        showingInfoText = true;
                        if (itemCount == 1) { curInfoSpr = &info1Spr; }
                        else                { curInfoSpr = &info2Spr; }
                    }
                }
            }
        }
        else if (runningTextDialogue) {
            player.StopWalkEffect();
            textDialogue.Update(dt);
            if (textDialogue.Done() &&
                input.MouseClicked())
            {
                runningTextDialogue = false;
            }
        }
        // showingInfoText == true
        else {
            player.StopWalkEffect();
            if (input.MouseClicked()) {
                showingInfoText = false;
                if (itemCount == 2) { unlockedEffect.Play(); }
            }
        }
        
        // DRAW --------------------------------------------------
        tileMapShader.Use();
        map.Draw(render, player.GetViewMat(), tileMapShader);
        
        if (!brainCollected) {
            modelShader->Use();
            brainModel.Draw(render, player.GetViewMat(), *modelShader);
        }
        if (!drillCollected) {
            modelShader->Use();
            drillModel.Draw(render, player.GetViewMat(), *modelShader);
        }
        
        if (runningTextDialogue) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            textDialogue.Draw(render);
        }
        else if (showingInfoText) {
            curInfoSpr->Draw(
                render,
                50, 50
            );
        }
        else if (playerNextToDoor) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            dialogueFont.PrintString(
                render,
                "Mouse Click: Open",
                150+10, S_HEIGHT - dialogueBg.GetHeight() + 50
            );
        }
        else if (playerNextToBrain || playerNextToDrill) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            dialogueFont.PrintString(
                render,
                "Mouse Click: Inspect",
                150+10, S_HEIGHT - dialogueBg.GetHeight() + 50
            );
        }
        
        player.Update();
        input.Update();
        render.Update();
    }

    player.StopWalkEffect();
    
    quit = false;
    bool passed = false;
    runningTextDialogue = true;
    textDialogue.SetText("In which state was the hospital?");
    textDialogue.SetTextSpeed(0.025f);
    textDialogue.SetX(150+10);
    textDialogue.SetY(S_HEIGHT - dialogueBg.GetHeight() + 50);
    textDialogue.SetFont(&dialogueFont);
    while (!input.Quit() && !quit)
    {
        float dt = timer.Update();
        render.Clear();
        
        // LOGIC --------------------------------------------------
        if (runningTextDialogue) {
            textDialogue.Update(dt);
            if (textDialogue.Done() &&
                input.MouseClicked())
            {
                runningTextDialogue = false;
            }
        }
        // User input select
        else {
            if (input.MouseClicked()) {
                if (MouseOverlapsNewJersey(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    passed = false;
                    quit = true;
                }
                else if (MouseOverlapsConnecticut(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    passed = false;
                    quit = true;
                }
                else if (MouseOverlapsMassachusets(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    passed = false;
                    quit = true;
                }
                else if (MouseOverlapsRhodeIsland(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    passed = true;
                    quit = true;
                }
            }
        }
        
        // DRAW ---------------------------------------------------
        tileMapShader.Use();
        map.Draw(render, player.GetViewMat(), tileMapShader);
        if (runningTextDialogue) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            textDialogue.Draw(render);
        }
        // user question guess input
        else {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            DrawQuestionChoices(dialogueFont, dialogueFontColored);
            scalpelSprite.Draw(render, input.MouseX(), input.MouseY());
        }
        
        player.Update();
        input.Update();
        render.Update();
    }
    
    return passed;
}
