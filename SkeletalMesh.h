#ifndef SKELETAL_MESH_H_INCLUDED
#define SKELETAL_MESH_H_INCLUDED

#include <vector>

#include <VertexBuffer.h>
#include <MatrixPalette.h>

class SkeletalMesh
{
public:
    
    SkeletalMesh();
    
    void Draw(class Renderer& render, class Shader& shader);
    
    void Update(const float dt);
    
    void SetSkeleton(const class Skeleton* sk) { mSkeleton = sk; }
    void SetVertexBuffer(const class VertexBuffer* vb) { mMesh = vb; }
    
    // Play an animation. returns the length of the animation
    float PlayAnimation(const class Animation* anim, float playRate = 1.0f);
    
    // Returns current object space position of the bone of the current pose
    Vec3 GetBonePosition(int index);
    
private:
    MatrixPalette mPalette;
    const class Skeleton* mSkeleton;
    const class Animation* mAnimation;
    const class VertexBuffer* mMesh;
    float mAnimPlayRate;
    float mAnimTime;
    std::vector<Mat4> mCurrentPoses;
    std::vector<Vec3> mCurrentBonePositions;
    // TODO - bone debug shader
    // static Shader BoneDebugShader;
    VertexBuffer mBoneVertBuff;
    
    void ComputeMatrixPalette();
    void DrawBonePositions(Mat4& viewProjMat);
};

#endif // SKELETAL_MESH_H_INCLUDED
