#ifndef MUSIC_H_INCLUDED
#define MUSIC_H_INCLUDED

#include <iostream>
#include <string>
#include <SDL2/SDL_mixer.h>

class Music
{
public:
    Music();
    ~Music();
    
    void Load(const std::string& fileName);
    
    void Play(const bool loop);
    inline void Stop() { Mix_HaltMusic(); }
    
private:
    Mix_Music* mMusic;
};

#endif // MUSIC_H_INCLUDED

