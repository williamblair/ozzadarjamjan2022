#ifndef RENDERER_H_INCLUDED
#define RENDERER_H_INCLUDED

#include <iostream>

#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#include <Math.h>
#include <VertexBuffer.h>
#include <Texture.h>

class Renderer
{
public:
    Renderer();
    ~Renderer();
    
    void Init(int width, int height, const char* title);
    void SetBackgroundColor(unsigned char r, unsigned char g, unsigned char b) {
        mBGRed = float(r) / 255.0f;
        mBGGreen = float(g) / 255.0f;
        mBGBlue = float(b) / 255.0f;
    }
    
    void Clear();
    void Update();

    void DrawVertexBuffer(const VertexBuffer& vb) {
        if (vb.mNumIndices == 0) {
            glBindVertexArray(vb.mVAO);
            glDrawArrays(GL_TRIANGLES, 0, vb.mNumVertices);
            glBindVertexArray(0);
        }
        else {
            glBindVertexArray(vb.mVAO);
            glDrawElements(GL_TRIANGLES, vb.mNumIndices, GL_UNSIGNED_INT, 0);
            glBindVertexArray(0);
        }
    }
    
    inline Mat4& GetPerspMat() { return mPerspMat; }
    inline int GetWidth() const { return mWidth; }
    inline int GetHeight() const { return mHeight; }
    
    void SetTexture(const Texture& t) {
        //std::cout << "Setting texture: " << t.mTexId << std::endl;
        glBindTexture(GL_TEXTURE_2D, t.mTexId);
    }

private:
    SDL_Window* mWindow;
    SDL_GLContext mContext;
    int mWidth;
    int mHeight;
    
    float mBGRed;
    float mBGGreen;
    float mBGBlue;
    
    Mat4 mPerspMat;
};

#endif // RENDERER_H_INCLUDED
