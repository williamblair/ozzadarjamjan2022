#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

#include <string>
#include <iostream>

#include <GL/glew.h>
#include <stb_image.h>

class Texture
{
    
friend class Renderer;
    
public:
    
    Texture();
    ~Texture();
    
    bool Load(const std::string& fileName);
    
    int GetWidth() const { return mWidth; }
    int GetHeight() const { return mHeight; }
    
private:
    int mWidth;
    int mHeight;
    int mNumChannels;
    GLuint mTexId;
};

#endif // TEXTURE_H_INCLUDED
