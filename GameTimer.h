#ifndef GAME_TIMER_H_INCLUDED
#define GAME_TIMER_H_INCLUDED

#include <SDL2/SDL.h>

class GameTimer
{
public:
    GameTimer();
    
    float Update();
    
private:
    Uint32 mLastTicks;
};

#endif
