#include <AssimpModel.h>
#include <limits>

bool    AssimpModel::DefaultTexLoaded = false;
Texture AssimpModel::DefaultTexture;

AssimpModel::AssimpModel() :
    mPosition(Vec3(0.0f, 0.0f, 0.0f)),
    mRotation(Math::angleAxis(0, Vec3(1.0f, 0.0f, 0.0f))),
    mScale(1.0f, 1.0f, 1.0f),
    mTexture(nullptr)
{
    UpdateModelMat();
}

AssimpModel::~AssimpModel()
{
}

void AssimpModel::Load(const std::string& fileName)
{
    Assimp::Importer import;
    const aiScene* scene = import.ReadFile(
        fileName,
        aiProcess_Triangulate | aiProcess_FlipUVs
    );
    
    if (!scene ||
        scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE ||
        !scene->mRootNode)
    {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "Assimp failed to import file: " << fileName << std::endl;
        throw std::runtime_error("Assimp failed to import file");
    }
    
    mDirectory = fileName.substr(0, fileName.find_last_of('/'));
    
    mMinPos = Math::Vec3FromVals(
        std::numeric_limits<float>::max(),
        std::numeric_limits<float>::max(),
        std::numeric_limits<float>::max()
    );
    mMaxPos = Math::Vec3FromVals(
        std::numeric_limits<float>::min(),
        std::numeric_limits<float>::min(),
        std::numeric_limits<float>::min()
    );
    ProcessNode(scene->mRootNode, scene);
    UpdateAabb();
    
    if (!DefaultTexLoaded) {
        LoadDefaultTex();
    }
}

void AssimpModel::ProcessNode(
    aiNode* node, 
    const aiScene* scene)
{
    // process node meshes, if any
    for (size_t i = 0; i < node->mNumMeshes; ++i)
    {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        ProcessMesh(mesh, scene);
    }
    
    // process node children, if any
    for (size_t i = 0; i < node->mNumChildren; ++i)
    {
        ProcessNode(node->mChildren[i], scene);
    }
}

void AssimpModel::ProcessMesh(
    aiMesh* mesh, 
    const aiScene* scene)
{
    std::cout << "Adding vert buf: " << mVertBufs.size()+1 << std::endl;
    mVertBufs.push_back(std::make_shared<VertexBuffer>());
    VertexBuffer& vertBuf = *(*mVertBufs.rbegin());
    std::vector<float> vertices;
    std::vector<unsigned int> indices;
    
    for (size_t i = 0; i < mesh->mNumVertices; ++i)
    {
        Vec3 pos;
        // TODO - add normals also
        // Vec3 norm
        Vec2 texCoord;
        
        pos = Math::Vec3FromVals(
            mesh->mVertices[i].x,
            mesh->mVertices[i].y,
            mesh->mVertices[i].z
        );
        
        // Update collision AABB values
        if (mesh->mVertices[i].x < mMinPos.x) {
            mMinPos.x = mesh->mVertices[i].x;
        }
        if (mesh->mVertices[i].y < mMinPos.y) {
            mMinPos.y = mesh->mVertices[i].y;
        }
        if (mesh->mVertices[i].z < mMinPos.z) {
            mMinPos.z = mesh->mVertices[i].z;
        }
        if (mesh->mVertices[i].x > mMaxPos.x) {
            mMaxPos.x = mesh->mVertices[i].x;
        }
        if (mesh->mVertices[i].y > mMaxPos.y) {
            mMaxPos.y = mesh->mVertices[i].y;
        }
        if (mesh->mVertices[i].z > mMaxPos.z) {
            mMaxPos.z = mesh->mVertices[i].z;
        }
        
        // TODO - norm
        //
        
        if (mesh->mTextureCoords[0]) {
            texCoord.x = mesh->mTextureCoords[0][i].x;
            texCoord.y = mesh->mTextureCoords[0][i].y;
        }
        else {
            texCoord.x = 0.0f;
            texCoord.y = 0.0f;
        }
        
        vertices.push_back(pos.x);
        vertices.push_back(pos.y);
        vertices.push_back(pos.z);
        // TODO - norm
        //
        vertices.push_back(texCoord.x);
        vertices.push_back(texCoord.y);
    }
    
    for (size_t i = 0; i < mesh->mNumFaces; ++i)
    {
        aiFace face = mesh->mFaces[i];
        for (size_t j = 0; j < face.mNumIndices; ++j)
        {
            indices.push_back(face.mIndices[j]);
        }
    }
    
    // TODO - materials
    //
    
    // 5 floats per vert (pos, tex coord)
    if (indices.size() > 0) {
        vertBuf.Init(vertices, indices, 5);
    }
    else {
        vertBuf.Init(vertices, 5);
    }
}

void AssimpModel::Draw(Renderer& render, Mat4& viewMat, Shader& shader)
{
    // TODO - allow textures
    if (mTexture == nullptr) {
        render.SetTexture(DefaultTexture);
    }
    else {
        render.SetTexture(*mTexture);
    }
    shader.SetMat4(
        "uTransform",
        render.GetPerspMat() *
        viewMat *
        mModelMat
    
    );
    for (auto vb : mVertBufs) {
        render.DrawVertexBuffer(*vb);
    }
}

void AssimpModel::LoadDefaultTex()
{
    const std::string& defaultTex = "assets/defaulttex.png";
    DefaultTexture.Load(defaultTex);
}
