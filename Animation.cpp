#include <Animation.h>
#include <Skeleton.h>

#include <fstream>
#include <sstream>

#include <rapidjson/document.h>

bool Animation::Load(const std::string& fileName)
{
    std::ifstream file(fileName);
    if (!file.is_open()) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "File not found: animation: " << fileName << std::endl;
        return false;
    }
    
    std::stringstream fileStream;
    fileStream << file.rdbuf();
    std::string contents = fileStream.str();
    rapidjson::StringStream jsonStr(contents.c_str());
    rapidjson::Document doc;
    doc.ParseStream(jsonStr);
    
    if (!doc.IsObject()) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "Animation not valid json: " << fileName << std::endl;
        return false;
    }
    
    int ver = doc["version"].GetInt();
    
    // Check the metadata
    if (ver != 1) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "Animation invalid version " << fileName << std::endl;
        return false;
    }
    
    const rapidjson::Value& sequence = doc["sequence"];
    if (!sequence.IsObject()) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "Animation has no sequence: " << fileName << std::endl;
        return false;
    }
    
    const rapidjson::Value& frames = sequence["frames"];
    const rapidjson::Value& length = sequence["length"];
    const rapidjson::Value& bonecount = sequence["bonecount"];
    
    if (!frames.IsUint() || !length.IsDouble() || !bonecount.IsUint()) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "Anim sequence invalid: " << fileName << std::endl;
        return false;
    }
    
    mNumFrames = frames.GetUint();
    mDuration = length.GetDouble();
    mNumBones = bonecount.GetUint();
    mFrameDuration = mDuration / (mNumFrames - 1);
    
    mTracks.resize(mNumBones);
    
    const rapidjson::Value& tracks = sequence["tracks"];
    if (!tracks.IsArray()) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "Sequence missing track array: " << fileName << std::endl;
        return false;
    }
    
    for (rapidjson::SizeType i = 0; i < tracks.Size(); ++i)
    {
        if (!tracks[i].IsObject()) {
            std::cerr << __FILE__ << ": " << __LINE__ << ": "
                << "Animation invalid track: " << fileName << std::endl;
            return false;
        }
        
        size_t boneIndex = tracks[i]["bone"].GetUint();
        
        const rapidjson::Value& transforms = tracks[i]["transforms"];
        if (!transforms.IsArray()) {
            std::cerr << __FILE__ << ": " << __LINE__ << ": "
                << "Animation track missing transforms: " << fileName << std::endl;
            return false;
        }
        
        BoneTransform temp;
        
        if (transforms.Size() < mNumFrames) {
            std::cerr << __FILE__ << ": " << __LINE__ << ": "
                << "Animation transforms size < num frames: " << fileName << std::endl;
            return false;
        }
        
        for (rapidjson::SizeType j = 0; j < transforms.Size(); ++j)
        {
            const rapidjson::Value& rot = transforms[j]["rot"];
            const rapidjson::Value& trans = transforms[j]["trans"];
            
            if (!rot.IsArray() || !trans.IsArray()) {
                std::cerr << __FILE__ << ": " << __LINE__ << ": "
                    << "Animation invalid rotation or translation: " << fileName << std::endl;
                return false;
            }
            
            temp.rotation = Math::QuatFromVals(
                rot[0].GetDouble(),
                rot[1].GetDouble(),
                rot[2].GetDouble(),
                rot[3].GetDouble()
            );
            temp.translation = Math::Vec3FromVals(
                trans[0].GetDouble(),
                trans[1].GetDouble(),
                trans[2].GetDouble()
            );
            
            mTracks[boneIndex].emplace_back(temp);
        }
    }
    
    return true;
}

void Animation::GetGlobalPoseAtTime(
    std::vector<Mat4>& outPoses,
    const Skeleton* inSkeleton,
    float inTime) const
{
    if (outPoses.size() != mNumBones) {
        outPoses.resize(mNumBones);
    }
    
    // Figure out the current frame index and next frame
    // this assumes inTime is bounded by [0, AnimDuration]
    size_t frame = static_cast<size_t>(inTime / mFrameDuration);
    size_t nextFrame = frame + 1;
    // calculate fractional value between frame and next
    float pct = inTime / mFrameDuration - frame;
    
    // Setup the pose for the root
    if (mTracks[0].size() > 0) {
        // Interpolate between current frame's pose and next frame
        BoneTransform interp = BoneTransform::Interpolate(
            mTracks[0][frame],
            mTracks[0][nextFrame],
            pct
        );
        
        outPoses[0] = interp.ToMatrix();
    }
    else {
        outPoses[0] = Math::identity();
    }
    
    const std::vector<Skeleton::Bone>& bones = inSkeleton->GetBones();
    // Now setup the poses for the rest
    for (size_t bone = 1; bone < mNumBones; ++bone)
    {
        Mat4 localMat = Math::identity();
        if (mTracks[bone].size() > 0)
        {
            BoneTransform interp = BoneTransform::Interpolate(
                mTracks[bone][frame],
                mTracks[bone][nextFrame],
                pct
            );
            localMat = interp.ToMatrix();
        }
        
        outPoses[bone] = outPoses[bones[bone].mParent] * localMat;
    }
    
}
