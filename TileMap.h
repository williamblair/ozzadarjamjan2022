#ifndef TILE_MAP_H_INCLUDED
#define TILE_MAP_H_INCLUDED

#include <unordered_map>
#include <memory>

#include <Math.h>
#include <Renderer.h>
#include <Camera.h>
#include <Shader.h>
#include <VertexBuffer.h>
#include <Texture.h>
#include <AABB.h>

class TileMap
{
public:
    
    TileMap();
    ~TileMap();
    
    void Load(const std::string& fileName);
    void AddTexture(const int mapVal, const std::string& fileName);
    void AddCeilTexture(const std::string& fileName);

    void Draw(Renderer& render, Mat4& viewMat, Shader& shader);
    
    const Vec3& GetPosition() const { return mPosition; }
    int GetTileSize() const { return mTileSize; }
    
    Vec3 GetTilePosition(int row, int col) {
        return Vec3(
            col*mTileSize,
            0.0f,
            row*mTileSize
        );
    }
    
    std::vector<AABB>& GetColliders() { return mWallAabbs; }

private:
    int mRows;
    int mCols;
    int mTileSize;
    Vec3 mPosition;
    VertexBuffer mVertBuf;
    
    std::vector<std::vector<int>> mMap;
    std::unordered_map<int,int> mTexMap;
    std::vector<std::shared_ptr<Texture>> mTextures;
    Texture mCeilTexture;
    std::vector<AABB> mWallAabbs;
    
    void InitVertBuff();
    void InitAabbs();
};

#endif // TILE_MAP_H_INCLUDED
