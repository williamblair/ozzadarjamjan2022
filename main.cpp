#include <sstream>
#include <cstdlib>
#include <ctime>

#include <Renderer.h>
#include <GameTimer.h>
#include <Shader.h>
#include <VertexBuffer.h>
#include <Camera.h>
#include <Input.h>
#include <Texture.h>
#include <TileMap.h>
#include <Player.h>
#include <SoundEffect.h>
#include <Music.h>
#include <AssimpModel.h>
#include <FreetypeFont.h>
#include <Sprite.h>
#include <Line.h>
#include <TextDialogue.h>

#include <TitleScreen.h>
#include <GameOverScreen.h>
#include <EndGameScreen.h>
#include <NextLevelScreen.h>
#include <Level1.h>
#include <Level2.h>
#include <Level3.h>
#include <Level4.h>

int S_WIDTH = 800;
int S_HEIGHT = 600;

// MinGW SDL error thing...
#ifdef __WIN32
#undef main
#endif

// Globals used by other gameplay functions
Renderer render;
GameTimer timer;
Input input;
Music BgMusic;

int main(int argc, char **argv)
{
    
    srand(time(0));
    
    try
    {
        render.Init(800, 600, "Every Death is Replayed");
        render.SetBackgroundColor(30, 40, 50);
        
        BgMusic.Load("assets/heartbeatloop.wav");
        
        while (!input.Quit())
        {
            int userSelect = TitleScreen();
            if (input.Quit() || 
                userSelect == TITLE_SELECT_QUIT)
            {
                break;
            }
            NextLevelScreen(1);
            if (input.Quit()) { break; }
            BgMusic.Play(true);
            bool passed = Level1();
            if (input.Quit()) { break; }
            if (!passed) {
                BgMusic.Stop();
                GameOverScreen();
                continue;
            }
            BgMusic.Stop();
            NextLevelScreen(2);
            if (input.Quit()) { break; }
            BgMusic.Play(true);
            passed = Level2();
            if (input.Quit()) { break; }
            if (!passed) {
                BgMusic.Stop();
                GameOverScreen();
                continue;
            }
            BgMusic.Stop();
            NextLevelScreen(3);
            if (input.Quit()) { break; }
            BgMusic.Play(true);
            passed = Level3();
            if (input.Quit()) { break; }
            if (!passed) {
                BgMusic.Stop();
                GameOverScreen();
                continue;
            }
            BgMusic.Stop();
            NextLevelScreen(4);
            if (input.Quit()) { break; }
            BgMusic.Play(true);
            passed = Level4();
            if (input.Quit()) { break; }
            if (!passed) {
                BgMusic.Stop();
                GameOverScreen();
                continue;
            }
            BgMusic.Stop();
            EndGameScreen();
        }
    }
    catch (std::runtime_error& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    return 0;
}
