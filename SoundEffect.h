#ifndef SOUND_EFFECT_H_INCLUDED
#define SOUND_EFFECT_H_INCLUDED

#include <iostream>
#include <string>
#include <SDL2/SDL_mixer.h>

class SoundEffect
{
public:
    SoundEffect();
    ~SoundEffect();
    
    void Load(const std::string& fileName);
    
    void Play(int channel = -1, bool loop = false);
    void Stop(int channel) {
        Mix_HaltChannel(channel);
    }

private:
    Mix_Chunk* mChunk;
};

#endif // SOUND_EFFECT_H_INCLUDED
