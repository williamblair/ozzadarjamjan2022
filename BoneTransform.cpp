#include <BoneTransform.h>

Mat4 BoneTransform::ToMatrix() const
{
    // TODO - this may not be accurate...
    Mat4 rot = Math::QuatToMat4(rotation);
    Mat4 trans = Math::translate(translation);
    
    //return rot * trans;
    return trans * rot;
}

BoneTransform 
BoneTransform::Interpolate(
    const BoneTransform& a,
    const BoneTransform& b, 
    float f)
{
    BoneTransform retVal;
    retVal.rotation = Math::Slerp(a.rotation, b.rotation, f);
    retVal.translation = Math::Lerp(a.translation, b.translation, f);
    return retVal;
}
