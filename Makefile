CC = mingw32-g++
#CC = g++
TARGET = main
#CFLAGS = -g -O3 -std=c++11
CFLAGS = -O3 -mwindows -std=c++11
INCDIRS = -I.
#INCDIRS = -I. -I/usr/include/freetype2
LIBDIRS =
LIBS = -lassimp -lfreetype -lopengl32 -lglew32 -lSDL2 -lSDL2_image -lSDL2_mixer
#LIBS = -lassimp -lfreetype -lGL -lGLU -lGLEW -lSDL2 -lSDL2_image -lSDL2_mixer
SOURCES = main.cpp \
          Renderer.cpp \
          Input.cpp \
          Texture.cpp \
          VertexBuffer.cpp \
          Camera.cpp \
          GameTimer.cpp \
          MoveComponent.cpp \
          Shader.cpp \
          StbImageImpl.cpp \
          TileMap.cpp \
          Player.cpp \
          SoundEffect.cpp \
          AssimpModel.cpp \
          FreetypeFont.cpp \
          Sprite.cpp \
          TextDialogue.cpp \
          TitleScreen.cpp \
          Music.cpp \
          Level1.cpp \
          Level2.cpp \
          Level3.cpp \
          Level4.cpp \
          GameOverScreen.cpp \
    	  EndGameScreen.cpp \
    	  NextLevelScreen.cpp

all:
	$(CC) $(CFLAGS) $(SOURCES) -o $(TARGET) $(INCDIRS) $(LIBDIRS) $(LIBS)

