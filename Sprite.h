#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <string>

#include <Texture.h>
#include <VertexBuffer.h>
#include <Math.h>
#include <Renderer.h>
#include <Shader.h>

class Sprite
{
public:

    Sprite();
    ~Sprite();
    
    void Load(const std::string& fileName, int screenW, int screenH);

    void SetScreenSize(int w, int h) { 
        mScreenSize.x = w; mScreenSize.y = h;
        UpdateVertBuf();
    }
    void SetUVWH(int u, int v, int w, int h) {
        mUv.x = u;
        mUv.y = v;
        mWh.x = w;
        mWh.y = h;
        UpdateVertBuf();
    }
    // rotation in degrees
    void SetRotation(float rot) { mRotation = rot; }
    void AddRotation(float amount) {
        mRotation += amount;
        while (mRotation < 0.0f) { mRotation += 360.0f; }
        while (mRotation > 360.0f) { mRotation -= 360.0f; }
    }
    void SetScale(float x, float y, float z) { mScale = Vec3(x, y, z); }
    void Draw(Renderer& render, int x, int y);
    
    float GetWidth() const { return mSpriteSize.x; }
    float GetHeight() const { return mSpriteSize.y; }

private:
    
    Texture mTexture;
    VertexBuffer mVertBuf;
    
    Vec2 mUv;
    Vec2 mWh;
    Vec2 mSpriteSize;
    Vec2 mScreenSize;
    float mRotation; // rotation in degrees
    Vec3 mScale;
    
    static bool SpriteShaderLoaded;
    static Shader SpriteShader;
    
    // screen pixels to OpenGL NDC coordinates
    inline float xPixelToNDCCoord(int pxVal)
    {
        return -1.0f + (2.0f * float(pxVal) / mScreenSize.x);
    }
    inline float yPixelToNDCCoord(int pxVal)
    {
        return -1.0f + (2.0f * (mScreenSize.y - float(pxVal)) / float(mScreenSize.y));
    }
    inline float pixWidthToNDCCoord(int pxVal)
    {
        return 2.0f * float(pxVal) / mScreenSize.x;
    }
    inline float pixHeightToNDCCoord(int pxVal)
    {
        return 2.0f * float(pxVal) / mScreenSize.y;
    }
    
    void UpdateVertBuf() {
        
        float leftU = mUv.x / mSpriteSize.x;
        float rightU = (mUv.x + mWh.x) / mSpriteSize.x;
        float topV = 1.0f - (mUv.y / mSpriteSize.y);
        float bottomV = 1.0f - ((mUv.y + mWh.y) / mSpriteSize.y);
        
        float ndcWidth = pixWidthToNDCCoord(mWh.x);
        float ndcHeight = pixHeightToNDCCoord(mWh.y);
        
        float leftX = -1.0f;
        float rightX = -1.0f + ndcWidth;
        float topY = 1.0f;
        float bottomY = 1.0f - ndcHeight;
        
        std::vector<float> updatedVerts = {
            // pos              uv
            leftX, bottomY,     leftU, bottomV,     // bottom left
            rightX, bottomY,    rightU, bottomV,     // bottom right
            rightX, topY,       rightU, topV,     // top right
            
            leftX, bottomY,     leftU, bottomV,     // bottom left
            rightX, topY,       rightU, topV,     // top right
            leftX, topY,        leftU, topV      // top left
        };
        
        mVertBuf.UpdateData(updatedVerts);
    }
};

#endif // SPRITE_H_INCLUDED
