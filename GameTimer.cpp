#include <GameTimer.h>

GameTimer::GameTimer() :
    mLastTicks(0)
{
}

float GameTimer::Update()
{
    Uint32 curTicks = SDL_GetTicks();
    
    if (mLastTicks == 0) {
        float dt = 0.0f;
        mLastTicks = curTicks;
        return dt;
    }
    
    Uint32 dtMilli = curTicks - mLastTicks;
    float dt = float(dtMilli) / 1000.0f;
    mLastTicks = curTicks;
    return dt;
}

