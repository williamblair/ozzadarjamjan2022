#include <TextDialogue.h>

TextDialogue::TextDialogue() :
    mFont(nullptr),
    mX(0),
    mY(0),
    mW(0),
    mText(""),
    mSpeed(0.0f),
    mDone(false),
    mCharIndex(0),
    mTimeCounter(0.0f)
{
}

TextDialogue::~TextDialogue()
{
}

void TextDialogue::Update(const float dt)
{
    mTimeCounter += dt;
    if (mTimeCounter >= mSpeed) {
        while (mTimeCounter >= mSpeed) { mTimeCounter -= mSpeed; }

        if (mCharIndex < mText.size()) {
            ++mCharIndex;
            mCurText = mText.substr(0, mCharIndex);
            if (mCharIndex >= mText.size()) {
                mDone = true;
            }
        }
    }
}

