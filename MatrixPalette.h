#ifndef MATRIX_PALETTE_H_INCLUDED
#define MATRIX_PALETTE_H_INCLUDED

#include <Math.h>

const size_t MAX_SKELETON_BONES = 96;

struct MatrixPalette
{
    Mat4 mEntry[MAX_SKELETON_BONES];
};

#endif // MATRIX_PALETTE_H_INCLUDED
