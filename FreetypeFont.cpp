#include <FreetypeFont.h>
#include <iostream>
#include <vector>

bool FreetypeFont::FontShaderLoaded = false;
Shader FreetypeFont::FontShader;

#define FONT_VERTEX_SHADER "Shaders/FontVertex.glsl"
#define FONT_FRAGMENT_SHADER "Shaders/FontFragment.glsl"

// screen pixels to OpenGL NDC coordinates
static inline float xPixelToNDCCoord(int pxVal, int sWidth)
{
    return -1.0f + (2.0f * float(pxVal) / float(sWidth));
}
static inline float yPixelToNDCCoord(int pxVal, int sHeight)
{
    return -1.0f + (2.0f * float(sHeight - pxVal) / float(sHeight));
}
static inline float pxSizeToNDCSize(int pxSize, int sSize)
{
    return 2.0f * float(pxSize) / float(sSize);
}

FreetypeFont::FreetypeFont() :
    mSize(0),
    mSWidth(0),
    mSHeight(0),
    mFontName("")
{
}

FreetypeFont::~FreetypeFont()
{
    glDeleteTextures(128, mTexIDs);
}

void FreetypeFont::Load(
    const std::string& fontName,
    int screenWidth, int screenHeight,
    unsigned char r,
    unsigned char g,
    unsigned char b,
    int fontSize)
{
    this->mSize = fontSize;
    this->mFontName = fontName;
    this->mSWidth = screenWidth;
    this->mSHeight = screenHeight;
    
    if (!FontShaderLoaded)  {
        FontShader.Init(FONT_VERTEX_SHADER, FONT_FRAGMENT_SHADER);
        FontShaderLoaded = true;
    }
    
    // TODO - statically load this once per class
    FT_Library library; // freetype library instance

    if (FT_Init_FreeType(&library))
    {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "failed to init FreeType " << std::endl;
        throw std::runtime_error("Failed to init freetype");
    }

    FT_Face fontInfo;
    if (FT_New_Face(library, fontName.c_str(), 0, &fontInfo))
    {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "failed to load font " << fontName << std::endl;
        throw std::runtime_error("Failed to load font");
    }

    // Freetype heights are 1/64th pixel size
    // 96x96 is dots per inch (dpi)
    FT_Set_Char_Size(
        fontInfo,
        fontSize * 64, fontSize * 64,
        96, 96
    );

    glGenTextures(128, mTexIDs);

    for (unsigned char ch = 0; ch < 128; ++ch)
    {
        if (!genCharacterTexture(ch, fontInfo, r,g,b))
        {
            std::cerr << __FILE__ << ": " << __LINE__ << ": "
                << ": failed to gen texture for char "
                << ch << std::endl;
            throw std::runtime_error("Failed to gen char texture");
        }
    }

    FT_Done_Face(fontInfo);
    FT_Done_FreeType(library);

    // will be updated for each character anyways
    float screenFontSize = pxSizeToNDCSize(fontSize, screenWidth);
    std::vector<float> vertices = {
        // position                     tex coord
        0.0f, 0.0f,                     0.0f, 1.0f,// bottom left
        screenFontSize, 0.0f,           1.0f, 1.0f, // bottom right
        screenFontSize, screenFontSize, 1.0f, 0.0f, // top right
        
        0.0f, 0.0f,                     0.0f, 1.0f, // bottom left
        screenFontSize, screenFontSize, 1.0f, 0.0f, // top right
        0.0f, screenFontSize,           0.0f, 0.0f  // top left
    };
    
    // 4 floats per vertex (x,y, u,v)
    mVertBuf.Init(vertices, 4);
}

void FreetypeFont::PrintString(
    Renderer& render, 
    const std::string& str,
    float x, float y)
{
    FontShader.Use();
    
    //GLuint aPosition = FontShader.GetAttribute("aPosition");
    //GLuint aTexCoord0 = FontShader.GetAttribute("aTexCoord0");
    
    FontShader.SetInt("uTexture0", 0);

    //glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);

    float normX = xPixelToNDCCoord(x, mSWidth);
    float normY = yPixelToNDCCoord(y, mSHeight);

    Vec2 charTransform(normX, normY);
    for (size_t i = 0; i < str.size(); ++i)
    {
        int ch = int(str[i]);
        
        float normWidth = pxSizeToNDCSize(
            mGlyphDimensions[ch].first,
            mSWidth
        );
        float normHeight = pxSizeToNDCSize(
            mGlyphDimensions[ch].second,
            mSHeight
        );
        std::vector<float> vertices = {
            // position            tex coord
            0.0f, 0.0f,            0.0f, 1.0f,// bottom left
            normWidth, 0.0f,       1.0f, 1.0f, // bottom right
            normWidth, normHeight, 1.0f, 0.0f, // top right
            
            0.0f, 0.0f,            0.0f, 1.0f, // bottom left
            normWidth, normHeight, 1.0f, 0.0f, // top right
            0.0f, normHeight,      0.0f, 0.0f  // top left
        };      
        mVertBuf.UpdateData(vertices);
        
        glBindTexture(GL_TEXTURE_2D, mTexIDs[ch]);
        
        FontShader.SetVec2("uTranslate", charTransform);
        
        render.DrawVertexBuffer(mVertBuf);
        
        float advance = pxSizeToNDCSize(mGlyphAdvances[ch], mSWidth);
        charTransform.x += advance;
    }

    //glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    
    //glDisableVertexAttribArray(aPosition);
    //glDisableVertexAttribArray(aTexCoord0);
}

bool FreetypeFont::genCharacterTexture(unsigned char ch, FT_Face fontInfo,
    unsigned char r,
    unsigned char g,
    unsigned char b)
{
    if (FT_Load_Glyph(fontInfo,
                      FT_Get_Char_Index(fontInfo, ch),
                      FT_LOAD_DEFAULT))
    {
        return false;
    }

    FT_Glyph glyph;
    if (FT_Get_Glyph(fontInfo->glyph, &glyph))
    {
        return false;
    }

    if (FT_Glyph_To_Bitmap(&glyph, ft_render_mode_normal, 0, 1))
    {
        return false;
    }

    FT_BitmapGlyph bitmapGlyph = (FT_BitmapGlyph)glyph;

    int width = bitmapGlyph->bitmap.width ? bitmapGlyph->bitmap.width : 16;
    int rows = bitmapGlyph->bitmap.rows ? bitmapGlyph->bitmap.rows : 16;

    // allocate space for font texture (r,g,b,a)
    int imageSize = width * rows * 4;

    std::vector<unsigned char> imageData(imageSize);
    for (int i = 0; i < imageSize / 4; ++i)
    {
        unsigned char gray = 0;
        if (bitmapGlyph->bitmap.buffer)
        {
            gray = bitmapGlyph->bitmap.buffer[i];
        }
        
        //imageData[i*4] = gray;
        //imageData[i*4 + 1] = gray;
        //imageData[i*4 + 2] = gray;
        //imageData[i*4 + 3] = gray; // alpha
        imageData[i*4 + 0] = r; // alpha
        imageData[i*4 + 1] = g;
        imageData[i*4 + 2] = b;
        imageData[i*4 + 3] = gray;
    }

    glBindTexture(GL_TEXTURE_2D, mTexIDs[ch]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 width, rows,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 imageData.data());

    mGlyphDimensions[ch] = std::make_pair(width, rows);
    mGlyphPositions[ch] = std::make_pair(bitmapGlyph->left, bitmapGlyph->top);
    mGlyphAdvances[ch] = fontInfo->glyph->advance.x / 64;

    glBindTexture(GL_TEXTURE_2D, 0);

    return true;
}
