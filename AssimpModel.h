#ifndef ASSIMP_MODEL_H_INCLUDED
#define ASSIMP_MODEL_H_INCLUDED

#include <string>
#include <memory>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <Renderer.h>
#include <Math.h>
#include <VertexBuffer.h>
#include <Shader.h>
#include <Texture.h>
#include <AABB.h>

class AssimpModel
{
public:

    AssimpModel();
    ~AssimpModel();
    
    void SetPosition(const Vec3& pos) { 
        mPosition = pos;
        UpdateModelMat();
        UpdateAabb();
    }
    void SetRotation(const float angleRadians, const Vec3& axis) {
        mRotation = Math::angleAxis(angleRadians, axis);
        UpdateModelMat();
        UpdateAabb();
    }
    void SetScale(const Vec3& scale) { 
        mScale = scale;
        UpdateModelMat();
        UpdateAabb();
    }
    void SetTexture(Texture* t) { mTexture = t; }
    
    void Load(const std::string& fileName);
    
    void Draw(Renderer& render, Mat4& viewMat, Shader& shader);

    AABB& GetCollider() { return mAabb; }

private:
    Vec3 mPosition;
    Quat mRotation;
    Vec3 mScale;
    Mat4 mModelMat;
    std::vector<std::shared_ptr<VertexBuffer>> mVertBufs;
    Vec3 mMinPos;
    Vec3 mMaxPos;
    AABB mAabb;
    std::string mDirectory;
    Texture* mTexture;
    
    static bool    DefaultTexLoaded;
    static Texture DefaultTexture;
    
    void ProcessNode(
        aiNode* node, 
        const aiScene* scene);
    void ProcessMesh(
        aiMesh* mesh, 
        const aiScene* scene);
    void LoadDefaultTex();    
    
    void UpdateModelMat() {
        mModelMat = Math::translate(mPosition) *
            Math::QuatToMat4(mRotation) *
            Math::scale(mScale);
    }
    void UpdateAabb() {
        Vec3 tfrmMin = Math::Transform(
            /*Math::QuatToMat4(mRotation) **/
            Math::scale(mScale),
            mMinPos
        );
        Vec3 tfrmMax = Math::Transform(
            /*Math::QuatToMat4(mRotation) **/
            Math::scale(mScale),
            mMinPos
        );
        mAabb.min = tfrmMin;
        mAabb.max = tfrmMax;
        mAabb.pos = mPosition;
    }
};

#endif // ASSIMP_MODEL_H_INCLDED
