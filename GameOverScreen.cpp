#include <GameOverScreen.h>

#include <Renderer.h>
#include <GameTimer.h>
#include <Input.h>
#include <Sprite.h>
#include <SoundEffect.h>

// globals from main.cpp
extern int S_WIDTH;
extern int S_HEIGHT;
extern Renderer render;
extern GameTimer timer;
extern Input input;

void GameOverScreen()
{
    Sprite bgTex;
    Sprite textBg;
    Sprite gameOverText;
    Sprite textShadow;
    SoundEffect gameOverEffect;
    
    int textBgX = 0;
    int textBgY = 0;
    int textX = 450;
    int textY = 525;
    float curTextX = -50.0f;
    float curTextY = (float)textY;
    float curBgX = 0.0f;
    float curBgY = -200.0f;
    const float animSpeed = 150.0f; // pixels per second
    
    bgTex.Load("assets/hospitalhallway_bjedit.png", S_WIDTH, S_HEIGHT);
    textBg.Load("assets/hospitalhallway_textbg.png", S_WIDTH, S_HEIGHT);
    gameOverText.Load("assets/hospitalhallway_text.png", S_WIDTH, S_HEIGHT);
    textShadow.Load("assets/hospitalhallway_text_shadow.png", S_WIDTH, S_HEIGHT);
    
    gameOverEffect.Load("assets/gameovereffect.wav");
    gameOverEffect.Play();
    
    bool quit = false;
    while (!quit && !input.Quit())
    {
        float dt = timer.Update();
        render.Clear();
        
        // UPDATE --------------------------------------------------
        if (input.MouseClicked()) { quit = true; }
        
        if ((int)curTextX < textX) {
            curTextX += animSpeed * dt;
            if (curTextX >= (float)textX) {
                curTextX = textX;
            }
        }
        if ((int)curBgY < textBgY) {
            curBgY += animSpeed * dt;
            if (curBgY >= (float)textBgY) {
                curBgY = textBgY;
            }
        }
        
        // DRAW ----------------------------------------------------
        bgTex.Draw(render, 0,0);
        textBg.Draw(render, (int)curBgX, (int)curBgY);
        textShadow.Draw(
            render,
            (int)curTextX + 5 + (-2 + rand()%5),
            (int)curTextY + 5 + (-2 + rand()%5)
        );
        gameOverText.Draw(
            render,
            (int)curTextX + (-2 + rand()%5),
            (int)curTextY + (-2 + rand()%5)
        );
        
        input.Update();
        render.Update();
    }
}
