#ifndef SKELETON_H_INCLUDED
#define SKELETON_H_INCLUDED

#include <string>
#include <vector>
#include <iostream>

#include <Math.h>
#include <BoneTransform.h>

class Skeleton
{
public:

    // Definition for each bone in the skeleton
    struct Bone
    {
        BoneTransform mLocalBindPose;
        std::string mName;
        int mParent;
    };
    
    // Load from a file
    bool Load(const std::string& fileName);

    // Getter functions
    size_t GetNumBones() const { return mBones.size(); }
    const Bone& GetBone(size_t idx) const { return mBones[idx]; }
    const std::vector<Bone>& GetBones() const { return mBones; }
    const std::vector<Mat4>& GetGlobalInvBindPoses() const { return mGlobalInvBindPoses; }
    
protected:
    // Called automatically when the skeleton is loaded
    // computes the global inverse bind pose for each bone
    void ComputeGlobalInvBindPose();
    
private:
    // The bones in the skeleton
    std::vector<Bone> mBones;
    // The global inverse bind poses for each bone
    std::vector<Mat4> mGlobalInvBindPoses;
};

#endif // SKELETON_H_INCLUDED
