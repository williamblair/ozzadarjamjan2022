#ifndef INPUT_H_INCLUDED
#define INPUT_H_INCLUDED

#include <SDL2/SDL.h>

class Input
{
public:
    Input();
    ~Input();
    
    void Update();
    
    inline bool Quit() { return mQuit; }
    inline bool Forward() { return mForward; }
    inline bool Backward() { return mBackward; }
    inline bool Left() { return mLeft; }
    inline bool Right() { return mRight; }
    inline bool YawRotPositive() { return mYawPos; }
    inline bool YawRotNegative() { return mYawNeg; }
    
    inline int MouseMoveX() { return mMouseMoveX; }
    inline int MouseMoveY() { return mMouseMoveY; }
    
    inline int MouseX() { return mMouseX; }
    inline int MouseY() { return mMouseY; }
    
    inline bool MouseClicked() { return mMouseClicked; }
    inline bool MouseHeld() { return mMouseHeld; }
    
private:
    bool mQuit;
    bool mForward;
    bool mBackward;
    bool mLeft;
    bool mRight;
    bool mYawPos;
    bool mYawNeg;
    
    int mMouseMoveX;
    int mMouseMoveY;
    
    int mMouseX;
    int mMouseY;
    
    bool mMouseClicked;
    bool mMouseHeld;
};

#endif
