#ifndef FREETYPE_FONT_H_INCLUDED
#define FREETYPE_FONT_H_INCLUDED

#include <string>
#include <unordered_map>

#include <GL/glew.h>

// Freetype headers
#include <ft2build.h>
#include <freetype/freetype.h>
#include <freetype/ftglyph.h>
#include <freetype/ftoutln.h>
#include <freetype/fttrigon.h>

#include <Shader.h>
#include <Renderer.h>

class FreetypeFont
{
public:
    FreetypeFont();
    ~FreetypeFont();
    
    void Load(
        const std::string& fontName,
        int screenWidth, int screenHeight,
        unsigned char r = 255,
        unsigned char g = 255,
        unsigned char b = 255,
        int fontSize = 16
    );
    void PrintString(Renderer& render, const std::string& str, float x, float y);

    int GetWidth() { return mGlyphDimensions['A'].first; }
    int GetHeight() { return mGlyphDimensions['A'].second; }

private:
    GLuint mTexIDs[128]; // a texture for each character

    int mSize;
    int mSWidth;
    int mSHeight;
    std::string mFontName;

    VertexBuffer mVertBuf;
    
    bool genCharacterTexture(unsigned char ch, FT_Face fontInfo,
        unsigned char r,
        unsigned char g,
        unsigned char b);

    static bool FontShaderLoaded;
    static Shader FontShader;

    std::unordered_map<char, std::pair<int, int>> mGlyphDimensions;
    std::unordered_map<char, std::pair<int, int>> mGlyphPositions;
    std::unordered_map<char, int> mGlyphAdvances;
};

#endif // FREETYPE_FONT_H_INCLUDED

