#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

#include <Math.h>
#include <MoveComponent.h>

class Camera
{
public:
    virtual Mat4& GetViewMat() = 0;
};

class FPSCamera : public Camera
{
public:
    FPSCamera();
    ~FPSCamera();
    
    MoveComponent& GetMoveComponent() { return mMoveComponent; }
    Vec3& GetPosition() { return mMoveComponent.position; }
    Mat4& GetViewMat() { return mViewMat; }
    
    void Update();
    
private:
    MoveComponent mMoveComponent;
    Mat4 mViewMat;
};

class OrbitCamera : public Camera
{
public:
    
    OrbitCamera();
    
    Mat4& GetViewMat() { return mViewMat; }
    
    void SetMoveComponent(MoveComponent* mc) { mMoveComponent = mc; }
    MoveComponent* GetMoveComponent() { return mMoveComponent; }
    
    void SetDistance(const float d) { mDistance = d; }
    void AddDistance(const float d) { mDistance += d; }
    float GetDistance() const { return mDistance; }
    
    void AddPitch(const float amount) {
        mPitch += amount;
        mPitch = Math::Clamp(mPitch, -80.0f, 80.0f);
    }
    void AddYaw(const float amount) {
        mYaw += amount;
        while (mYaw < 0.0f) { mYaw += 360.0f; }
        while (mYaw > 360.0f) { mYaw -= 360.0f; }
    }
    
    void Update();
    
private:
    Mat4 mViewMat;
    MoveComponent* mMoveComponent; // the position we're following
    Vec3 mPosition;
    float mDistance; // how far away from the moveComponent target
    float mYaw;
    float mPitch;
};

class ThirdPersonCamera : public Camera
{
public:
    
    ThirdPersonCamera();
    
    void SetDistance(const float d) { mDistance = d; }
    void AddDistance(const float d) { mDistance += d; }
    float GetDistance() const { return mDistance; }
    
    MoveComponent& GetMoveComponent() { return mMoveComponent; }
    
    Mat4& GetViewMat() { return mViewMat; }
    
    void Update();
    
private:
    Mat4 mViewMat;
    MoveComponent mMoveComponent;
    float mDistance;
};

#endif // CAMERA_H_INCLUDED
