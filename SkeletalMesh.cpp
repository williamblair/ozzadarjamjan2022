#include <SkeletalMesh.h>
#include <VertexBuffer.h>
#include <Animation.h>
#include <Skeleton.h>
#include <Renderer.h>
#include <Shader.h>

SkeletalMesh::SkeletalMesh() :
    mSkeleton(nullptr),
    mAnimation(nullptr),
    mMesh(nullptr)
{
}

void SkeletalMesh::Draw(Renderer& render, Shader& shader)
{
    // TODO
    // shader->SetMatrixUniform("uWorldTransform", ...)
    
    // Set the matrix palette
    shader.SetMat4Array(
        "uMatrixPalette", 
        &mPalette.mEntry[0], 
        MAX_SKELETON_BONES
    );
    
    // TODO - specular power and stuff
    // shader->SetFloatUniform("uSpecPower", ...)
    
    // TODO - texture
    // Texture* t = mMesh->GetTexture()...
    
    if (mMesh) {
        render.DrawVertexBuffer(*mMesh);
    }
    
    // TODO - debug draw joint positions
}

void SkeletalMesh::Update(const float dt)
{
    if (!mAnimation || !mSkeleton)
    {
        return;
    }
    
    // Update animation time
    mAnimTime += dt * mAnimPlayRate;
    
    // Wrap around anim time if past duration
    while (mAnimTime > mAnimation->GetDuration()) {
        mAnimTime -= mAnimation->GetDuration();
    }
    
    // Recompute matrix palette
    ComputeMatrixPalette();
    
    // Get bone positions
    if (mCurrentBonePositions.size() != mCurrentPoses.size()) {
        mCurrentBonePositions.resize(mCurrentPoses.size());
    }
    for (size_t i = 0; i < mCurrentPoses.size(); ++i) {
        mCurrentBonePositions[i] = GetBonePosition(i);
    }
}

float SkeletalMesh::PlayAnimation(const Animation* anim, float playRate)
{
    mAnimation = anim;
    mAnimTime = 0.0f;
    mAnimPlayRate = playRate;
    
    if (!mAnimation) { return 0.0f; }
    
    ComputeMatrixPalette();
    
    return mAnimation->GetDuration();
}

Vec3 SkeletalMesh::GetBonePosition(int index)
{
    Mat4& pose = mCurrentPoses[index];
    Vec3 position = Math::Vec3FromVals(0.0f, 0.0f, 0.0f);
    
    position = Math::Transform(pose, position);
    
    return position;
}

void SkeletalMesh::ComputeMatrixPalette()
{
    const std::vector<Mat4>& globalInvBindPoses = mSkeleton->GetGlobalInvBindPoses();
    
    mAnimation->GetGlobalPoseAtTime(mCurrentPoses, mSkeleton, mAnimTime);
    
    // TODO - generate debug skeleton position buffers and stuff
    
    // Setup the aplette for each bone
    for (size_t i = 0; i < mSkeleton->GetNumBones(); ++i)
    {
        // Global inverse bind pose matrix times current pose matrix
        mPalette.mEntry[i] = mCurrentPoses[i] * globalInvBindPoses[i];
    }
}

void SkeletalMesh::DrawBonePositions(Mat4& viewProjMat)
{
    // TODO
    (void)viewProjMat;
}

