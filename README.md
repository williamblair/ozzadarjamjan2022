# Every Death is Replayed

My submission for the SNHU CS Hangout Winter Jam 2021.

This is a horror-ish game about medical failures, using real life events.

Articles used:

* https://www.caringlawyers.com/blog/13-disturbing-cases-of-medical-malpractice/
* https://leightonlaw.com/12-famous-medical-malpractice-cases/
* https://www.theguardian.com/society/2019/aug/23/americans-plastic-surgery-dominican-republic
* https://en.wikipedia.org/wiki/Jesica_Santillan

Inspiration:

* https://www.omniverse-plastikos.com/useful/what-happens-if-a-patient-dies-during-surgery.html

Libraries Used:

* SDL2
* OpenGL 3.3
* Assimp
* stb_image
* glm

Compiled With MinGW for windows, gcc 8.2.0.
Also briefly tested on Ubuntu 20.04

