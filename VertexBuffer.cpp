#include <VertexBuffer.h>
#include <Math.h>

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cstdint>
#include <cassert>

namespace
{
    union Vertex
    {
        float f;
        uint8_t b[4];
    };
}

VertexBuffer::VertexBuffer() :
    mVAO(0),
    mVBO(0),
    mEBO(0),
    mNumFloats(0),
    mNumVertices(0),
    mNumIndices(0)
{
}

VertexBuffer::~VertexBuffer()
{
    //std::cout << "Freeing vertex buffer" << std::endl;
    if (mNumFloats > 0) {
        glDeleteVertexArrays(1, &mVAO);
        glDeleteBuffers(1, &mVBO);
        glDeleteBuffers(1, &mEBO);
    }
}

void VertexBuffer::Init(std::vector<float>& vertices, int floatsPerVertex)
{
    glGenVertexArrays(1, &mVAO);
    glGenBuffers(1, &mVBO);
    glBindVertexArray(mVAO);
    
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    // TODO - static or dynamic draw
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(float), vertices.data(), GL_STATIC_DRAW);
    
    mNumVertices = vertices.size() / floatsPerVertex;
    mNumFloats = vertices.size();
    mNumIndices = 0;
    
    if (floatsPerVertex == 3)
    {
        // position attribute
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, floatsPerVertex*sizeof(float), (void*)0);
        glEnableVertexAttribArray(0); // expects shader location=0
    }
    else if (floatsPerVertex == 4)
    {
        // position attribute
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, floatsPerVertex*sizeof(float), (void*)0);
        glEnableVertexAttribArray(0); // expects shader location=0
        
        // texcoord attribute
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, floatsPerVertex*sizeof(float), (void*)(2*sizeof(float)));
        glEnableVertexAttribArray(1); // expects shader location=1
    }
    else if (floatsPerVertex == 5)
    {
        // position attribute
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, floatsPerVertex*sizeof(float), (void*)0);
        glEnableVertexAttribArray(0); // expects shader location=0
        
        // texcoord attribute
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, floatsPerVertex*sizeof(float), (void*)(3*sizeof(float)));
        glEnableVertexAttribArray(1); // expects shader location=0
    }
    else
    {
        std::cerr << __FILE__ << ": " << __LINE__ << ": unhandled floatsPerVertex: " << floatsPerVertex << std::endl;
        throw std::runtime_error("Unhandled floatsPerVertex, expected 3");
    }
    
    glBindVertexArray(0);
}

void VertexBuffer::Init(std::vector<float>& vertices, std::vector<unsigned int>& indices, int floatsPerVertex)
{
    glGenVertexArrays(1, &mVAO);
    glGenBuffers(1, &mVBO);
    glGenBuffers(1, &mEBO);
    glBindVertexArray(mVAO);
    
    int vertByteSize = floatsPerVertex * sizeof(float);
    
    // Upload vertex data
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    // TODO - static or dynamic draw
    glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(float), vertices.data(), GL_STATIC_DRAW);
    
    // Upload index data
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);
    
    mNumVertices = vertices.size() / floatsPerVertex;
    mNumFloats = vertices.size();
    mNumIndices = indices.size();
    
    if (floatsPerVertex == 5)
    {
        // position attribute
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, vertByteSize, (void*)0);
        glEnableVertexAttribArray(0); // expects shader location=0
        
        // tex coord attribute
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, vertByteSize, (void*)(3*sizeof(float)));
        glEnableVertexAttribArray(1); // expects shader location=1
    }
    else
    {
        std::cerr << __FILE__ << ": " << __LINE__ << ": unhandled floatsPerVertex: " << floatsPerVertex << std::endl;
        throw std::runtime_error("Unhandled floatsPerVertex, expected 10");
    }
    
    glBindVertexArray(0);
}

void VertexBuffer::Init(float* vertices, size_t verticesSize, std::vector<unsigned int>& indices, int floatsPerVertex)
{
    glGenVertexArrays(1, &mVAO);
    glGenBuffers(1, &mVBO);
    glGenBuffers(1, &mEBO);
    glBindVertexArray(mVAO);
    
    int vertByteSize = floatsPerVertex * sizeof(float);
    // TODO - not hardcode
    assert(floatsPerVertex == 10);
    if (floatsPerVertex == 10)
    {
        vertByteSize = 8*sizeof(float) + 8*sizeof(char);
    }
    
    // Upload vertex data
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    // TODO - static or dynamic draw
    glBufferData(GL_ARRAY_BUFFER, verticesSize*sizeof(float), vertices, GL_STATIC_DRAW);
    
    // Upload index data
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);
    
    mNumVertices = verticesSize / floatsPerVertex;
    mNumFloats = verticesSize;
    mNumIndices = indices.size();
    
    if (floatsPerVertex == 10)
    {
        // position attribute
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, vertByteSize, (void*)0);
        glEnableVertexAttribArray(0); // expects shader location=0
        
        // normal attribute
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, vertByteSize, (void*)(3*sizeof(float)));
        glEnableVertexAttribArray(1); // expects shader location=1
        
        // skin indices attribute
        glVertexAttribIPointer(2, 4, GL_UNSIGNED_BYTE, vertByteSize, (void*)(6*sizeof(float)));
        glEnableVertexAttribArray(2); // expects shader location=2
        
        // skin weights attribute
        // convert to floats (normalize = true)
        // for offset, 4 bytes = 1 float so offset is 7 floats
        glVertexAttribPointer(3, 4, GL_UNSIGNED_BYTE, GL_TRUE, vertByteSize, 
            (void*)(6*sizeof(float) + 4*sizeof(char)));
        glEnableVertexAttribArray(3); // expects shader location=3
        
        // texcoord attribute
        glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, vertByteSize, 
            (void*)(6*sizeof(float) + 8*sizeof(char)));
        glEnableVertexAttribArray(4); // expects shader location=4
    }
    else
    {
        std::cerr << __FILE__ << ": " << __LINE__ << ": unhandled floatsPerVertex: " << floatsPerVertex << std::endl;
        throw std::runtime_error("Unhandled floatsPerVertex, expected 10");
    }
    
    glBindVertexArray(0);
}

void VertexBuffer::UpdateData(std::vector<float>& vertices)
{
    glBindVertexArray(mVAO);
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glBufferSubData(
        GL_ARRAY_BUFFER,
        0,
        vertices.size() * sizeof(float),
        vertices.data()
    );
    glBindVertexArray(0);
}

