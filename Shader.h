#ifndef SHADER_H_INCLUDED
#define SHADER_H_INCLUDED

#include <iostream>
#include <string>
#include <fstream>

#include <GL/glew.h>

#include <Math.h>

class Shader
{
public:
    Shader();
    ~Shader();
    
    void Init(const std::string& vertexFile, const std::string& fragmentFile);
    
    inline void Use() { glUseProgram(mProgID); }
    inline GLuint GetProgID() { return mProgID; }
    
    // Uniform setters
    void SetMat4(const char* name, const Mat4& mat) {
        GLint loc = glGetUniformLocation(mProgID, name);
        if (loc < 0) {
            std::cerr << __FILE__ << ": " << __LINE__
                <<": failed to set mat4 uniform: " << name << std::endl;
            return;
        }
        glUniformMatrix4fv(
            loc, 
            1, 
            GL_FALSE,
            Math::mat4ToFloatPtr(mat)
        );
    }
    void SetMat4Array(const char* name, const Mat4* mats, const unsigned int count) {
        GLint loc = glGetUniformLocation(mProgID, name);
        if (loc < 0) {
            std::cerr << __FILE__ << ": " << __LINE__
                <<": failed to set mat4 uniform array: " << name << std::endl;
            return;
        }
        glUniformMatrix4fv(
            loc, 
            count, 
            GL_FALSE,
            (float*)mats
        );
    }
    void SetInt(const char* name, const int val) {
        GLint loc = glGetUniformLocation(mProgID, name);
        if (loc < 0) {
            std::cerr << __FILE__ << ": " << __LINE__
                <<": failed to set int uniform: " << name << std::endl;
            return;
        }
        glUniform1i(loc, val);
    }
    void SetVec2(const char* name, const Vec2& val) {
        GLint loc = glGetUniformLocation(mProgID, name);
        if (loc < 0) {
            std::cerr << __FILE__ << ": " << __LINE__
                <<": failed to set vec2 uniform: " << name << std::endl;
            return;
        }
        glUniform2fv(loc, 1, Math::vec2ToFloatPtr(val));
    }
    
    GLint GetAttribute(const std::string& name) {
        return glGetUniformLocation(mProgID, name.c_str());
    }
    
private:
    GLuint mProgID;

    std::string getShaderStr(const std::string filename);
};

#endif // SHADER_H_INCLUDED
