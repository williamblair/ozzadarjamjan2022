#include <SoundEffect.h>

SoundEffect::SoundEffect() :
    mChunk(nullptr)
{
}

SoundEffect::~SoundEffect()
{
    if (mChunk) {
        Mix_FreeChunk(mChunk);
    }
}

void SoundEffect::Load(const std::string& fileName)
{
    mChunk = Mix_LoadWAV(fileName.c_str());
    if (!mChunk) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "failed to load sound effect: " << Mix_GetError()
            << std::endl;
        throw std::runtime_error(Mix_GetError());
    }
}

void SoundEffect::Play(int channel, bool loop)
{
    if (Mix_PlayChannel(channel, mChunk, loop ? -1 : 0) == -1) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "failed to play sound effect: " << Mix_GetError()
            << std::endl;
        //throw std::runtime_error(Mix_GetError());
    }
}


