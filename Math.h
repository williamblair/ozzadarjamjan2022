#ifndef MATH_H_INCLUDED
#define MATH_H_INCLUDED

// MinGW build
#ifdef __WIN32
#define _USE_MATH_DEFINES
#endif
#include <cmath>
#include <memory.h>
#include <limits>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>

#define Vec2 glm::vec2
#define Vec3 glm::vec3
#define Vec4 glm::vec4
#define Mat4 glm::mat4
#define Quat glm::quat

namespace Math
{
    const float Pi = M_PI;
    const float TwoPi = Pi * 2.0f;
    const float PiOver2 = Pi / 2.0f;
    const float Infinity = std::numeric_limits<float>::infinity();
    const float NegInfinity = -std::numeric_limits<float>::infinity();
    
    inline float ToRadians(float degrees) {
        return degrees * Pi / 180.0f;
    }
    inline float ToDegrees(float radians) {
        return radians * 180.0f / Pi;
    }
    inline bool NearZero(float val, float epsilon = 0.001f) {
        if (fabs(val) < epsilon) {
            return true;
        }
        return false;
    }
    
    template<typename T>
    T Max(const T& a, const T& b) {
        return (a < b ? b : a);
    }
    template<typename T>
    T Min(const T& a, const T& b) {
        return (a < b ? a : b);
    }
    template<typename T>
    T Clamp(const T& value, const T& lower, const T& upper) {
        return Min(upper, Max(lower, value));
    }
    
    inline float Abs(float val) {
        return fabs(val);
    }
    inline float Cos(float val) {
        return cosf(val);
    }
    inline float Sin(float val) {
        return sinf(val);
    }
    inline float Tan(float val) {
        return tanf(val);
    }
    inline float Acos(float val) {
        return acosf(val);
    }
    inline float Atan(float val) {
        return atanf(val);
    }
    inline float Atan2(float y, float x) {
        return atan2f(y, x);
    }
    inline float Cot(float val) {
        return 1.0f / Tan(val);
    }
    inline float Lerp(float a, float b, float t) {
        return a + t * (b - a);
    }
    inline float Sqrt(float val) {
        return sqrtf(val);
    }
    inline float Fmod(float numer, float denom) {
        return fmodf(numer, denom);
    }
    
    inline Mat4 perspective(
        const float fovRadians,
        const float aspect,
        const float near,
        const float far
    )
    {
        return glm::perspective(fovRadians, aspect, near, far);
    }
    inline Mat4 lookAt(
        const Vec3& eyePos,
        const Vec3& targetPos,
        const Vec3& up
    )
    {
        return glm::lookAt(eyePos, targetPos, up);
    }
    inline Mat4 translate(const float x, const float y, const float z) {
        return glm::translate(glm::mat4(1.0f),glm::vec3(x,y,z));
    }
    inline Mat4 translate(const Vec3& v) {
        return glm::translate(glm::mat4(1.0f),v);
    }
    inline Mat4 scale(const Vec3& s) {
        return glm::scale(glm::mat4(1.0f), s);
    }
    inline Mat4 scale(const float x, const float y, const float z) {
        return glm::scale(glm::mat4(1.0f), glm::vec3(x,y,z));
    }
    inline Mat4 identity() {
        return glm::mat4(1.0f);
    }
    inline Mat4 inverse(const Mat4& mat) {
        return glm::inverse(mat);
    }
    inline const float* mat4ToFloatPtr(const Mat4& mat) {
        return glm::value_ptr(mat);
    }
    inline const float* vec2ToFloatPtr(const Vec2& vec) {
        return glm::value_ptr(vec);
    }
    
    inline Quat angleAxis(const float angleRadians, const Vec3& axis) {
        return glm::angleAxis(angleRadians, axis);
    }
    inline Quat Slerp(const Quat& a, const Quat& b, const float t) {
        return glm::slerp(a, b, t);
    }
    
    inline Vec3 Normalize(const Vec3& v) { return glm::normalize(v); }
    inline Quat Normalize(const Quat& q) { return glm::normalize(q); }
    
    inline Vec3 Cross(const Vec3& a, const Vec3& b) { return glm::cross(a,b); }
    inline Vec3 Lerp(const Vec3& a, const Vec3& b, const float t) { return glm::mix(a, b, t); }
    
    inline float LenSq(const Vec3& v) {
        return glm::length2(v);
    }
    
    inline Quat QuatFromVals(float x, float y, float z, float w) {
        return glm::quat(w, x, y, z);
    }
    inline Vec3 Vec3FromVals(float x, float y, float z) {
        return glm::vec3(x, y, z);
    }
    inline Vec3 Transform(const Mat4& m, const Vec3& v, const float w = 1.0f) {
        return glm::vec3(
            m * glm::vec4(v, w)
        );
    }
    inline Mat4 QuatToMat4(const Quat& q) {
        return glm::toMat4(glm::normalize(q));
    }
}


#endif
