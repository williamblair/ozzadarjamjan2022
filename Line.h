#ifndef LINE_H_INCLUDED
#define LINE_H_INCLUDED

struct Line
{
    float x1;
    float y1;
    float x2;
    float y2;
    
    float slope; // automatically calculated in constructor
    
    void Set(float x1, float y1, float x2, float y2)
    {
        this->x1 = x1;
        this->y1 = y1;
        this->x2 = x2;
        this->y2 = y2;
        
        // ensure x1 is always on the left
        if (x2 < x1)
        {
            float tmp = x1;
            x1 = x2;
            x2 = tmp;
            
            tmp = y1;
            y1 = y2;
            y2 = tmp;
        }
        
        slope = (-(y2 - y1)) / (x2 - x1);
    }
    
    // flipped since screen y=0 starts at top
    float CalcY(float x) {
        return y1 - slope * (x - x1);
    }
    // angle between x1,y1 and x2,y2
    float Angle() {
        return Math::Atan((-(y2 - y1)) / (x2 - x1));
    }
};

#endif // LINE_H_INCLUDED
