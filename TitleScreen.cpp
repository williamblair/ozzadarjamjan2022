#include <TitleScreen.h>

#include <Renderer.h>
#include <GameTimer.h>
#include <Shader.h>
#include <VertexBuffer.h>
#include <Camera.h>
#include <Input.h>
#include <Texture.h>
#include <SoundEffect.h>
#include <FreetypeFont.h>
#include <Sprite.h>
#include <TextDialogue.h>
#include <Music.h>

// globals from main.cpp
extern int S_WIDTH;
extern int S_HEIGHT;
extern Renderer render;
extern GameTimer timer;
extern Input input;

inline bool mouseOverlapsBegin(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 200 &&
        input.MouseX() <= (200 + strlen("BEGIN")*24) &&
        input.MouseY() <= (S_HEIGHT - 75) &&
        input.MouseY() >= ((S_HEIGHT - 75) - fontHeight));
}

inline bool mouseOverlapsQuit(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 500 &&
        input.MouseX() <= (500 + strlen("BEGIN")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 75) &&
        input.MouseY() >= ((S_HEIGHT - 75) - fontHeight));
}

void DrawBeginQuit(
    FreetypeFont& font,
    FreetypeFont& fontColored)
{
    FreetypeFont* f = mouseOverlapsBegin(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "BEGIN", 200, S_HEIGHT - 75);
    
    f = mouseOverlapsQuit(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "QUIT", 500, S_HEIGHT - 75);
}

int TitleScreen()
{
    Sprite titleScreens[4];
    Sprite scalpelSprite;
    SoundEffect titleChangeEffect;
    Music bgMusic;
    FreetypeFont menuFont;
    FreetypeFont menuFontHilighted;
    
    titleScreens[0].Load("assets/title1.png", S_WIDTH, S_HEIGHT);
    titleScreens[1].Load("assets/title2.png", S_WIDTH, S_HEIGHT);
    titleScreens[2].Load("assets/title3.png", S_WIDTH, S_HEIGHT);
    titleScreens[3].Load("assets/title4.png", S_WIDTH, S_HEIGHT);
    Sprite* curTitleSprite = &titleScreens[0];
    
    scalpelSprite.Load("assets/scalpel.png", S_WIDTH, S_HEIGHT);

    titleChangeEffect.Load("assets/distortedguitar1.wav");
    bgMusic.Load("assets/guitarbackground.wav");

    menuFont.Load(
        "assets/Nervous.ttf",
        S_WIDTH, S_HEIGHT,
        255, 255, 255, 
        24
    );
    menuFontHilighted.Load(
        "assets/Nervous.ttf",
        S_WIDTH, S_HEIGHT,
        255, 255, 0, // yellow 
        24
    );

    // Screen intro animation
    float animCounter = 0.0f;
    float animSpeed = 1.25f; // 1 second per screen
    float nextTitleIndex = 1.0f;

    titleChangeEffect.Play();
    while (animCounter < animSpeed * 4 &&
           !input.Quit()) // 4  title screens
    {
        render.Clear();
        float dt = timer.Update();
        animCounter += dt;
        
        if (animCounter >= nextTitleIndex * animSpeed) {
            nextTitleIndex += 1.0f;
            if (curTitleSprite != &titleScreens[3]) { 
                ++curTitleSprite;
                titleChangeEffect.Play();
            }
        }
        
        // rendering
        curTitleSprite->Draw(render, 0,0); // takes up the entire screen


        input.Update();
        render.Update();
    }

    int selection = TITLE_SELECT_QUIT;
    bool quit = false;
    bgMusic.Play(true);
    while (!quit && !input.Quit())
    {
        render.Clear();
        float dt = timer.Update();

        // UPDATE --------------------------------------------------
        if (input.MouseClicked() &&
            mouseOverlapsBegin(menuFont.GetWidth(), menuFont.GetHeight()))
        {
            selection = TITLE_SELECT_BEGIN;
            quit = true;
        }
        else if (input.MouseClicked() &&
            mouseOverlapsQuit(menuFont.GetWidth(), menuFont.GetHeight()))
        {
            selection = TITLE_SELECT_QUIT;
            quit = true;
        }

        // DRAW ----------------------------------------------------
        titleScreens[3].Draw(render, 0,0);
        DrawBeginQuit(menuFont, menuFontHilighted);
        scalpelSprite.Draw(render, input.MouseX(), input.MouseY());

        input.Update();
        render.Update();
    }
    bgMusic.Stop();
    
    return selection;
}

