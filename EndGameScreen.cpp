#include <EndGameScreen.h>

#include <string>

#include <Renderer.h>
#include <GameTimer.h>
#include <Input.h>
#include <Music.h>
#include <SoundEffect.h>
#include <FreetypeFont.h>
#include <Sprite.h>

// globals from main.cpp
extern int S_WIDTH;
extern int S_HEIGHT;
extern Renderer render;
extern GameTimer timer;
extern Input input;
extern Music BgMusic;

// sequence of operations
enum class EndGameState
{
    QuestionText,
    QuestionPause,
    AnswerText,
    AnswerPause,
    AuthorText,
    AuthorPause,
    Done
};
EndGameState CurState = EndGameState::QuestionText;

static float timeCtr = 0.0f;
static const float textSpeed = 0.09f; // time per character
static const int EFFECT_CHANNEL = 1;

static std::string curQuestionStr = "W";

static void QuestionTextLogic(
    FreetypeFont& font,
    SoundEffect& typeWriterEffect,
    const float dt)
{
    static const std::string questionStr =
        "What happens when someone dies in the operating room?";
    
    if (curQuestionStr.size() != questionStr.size()) {
        timeCtr += dt;
        if (timeCtr >= textSpeed) {
            curQuestionStr += questionStr[curQuestionStr.size()];
            if (curQuestionStr[curQuestionStr.size()-1] != ' ') {
                typeWriterEffect.Play(EFFECT_CHANNEL, false);
            }
            
            while (timeCtr >= textSpeed) { timeCtr -= textSpeed; }
        }
    }
    
    if (curQuestionStr.size() == questionStr.size()) {
        CurState = EndGameState::QuestionPause;
        timeCtr = 0.0f;
    }
}

static std::vector<std::string> curAnswerStrs;

static void QuestionPauseLogic(const float dt)
{
    static const float PAUSE_TIME = 2.0f; // 2 seconds
    
    timeCtr += dt;
    if (timeCtr >= PAUSE_TIME) {
        timeCtr = 0;
        CurState = EndGameState::AnswerText;
        //curAnswerStrs.push_back("E"); // first char of first answerStr
        curAnswerStrs.push_back(""); // first char of first answerStr
    }
}

static void AnswerTextLogic(
    FreetypeFont& font,
    SoundEffect& typeWriterEffect,
    const float dt)
{
    static const std::string answerStrs[3] = {
        "Every death is replayed.",
        "As if the film were reversed, death would give back a pulse.",
        "But the clock is stubborn, unwilling to turn."
    };
    static size_t strIndex = 0;
    static const float inBetweenPause = 0.5f;
    static bool isPaused = false;
    
    timeCtr += dt;
    
    std::string& curAnswerStr = curAnswerStrs[strIndex];
    if (isPaused) {
        if (timeCtr >= inBetweenPause) {
            isPaused = false;
            timeCtr = 0.0f;
        }
    }
    else if (curAnswerStr.size() != answerStrs[strIndex].size()) {
        if (timeCtr >= textSpeed) {
            curAnswerStr += answerStrs[strIndex][curAnswerStr.size()];
            if (curAnswerStr[curAnswerStr.size()-1] != ' ') {
                typeWriterEffect.Play(EFFECT_CHANNEL, false);
            }
            
            while (timeCtr >= textSpeed) { timeCtr -= textSpeed; }
        }
    }
    else {
        ++strIndex;
        if (strIndex >= 3) {
            CurState = EndGameState::AnswerPause;
            timeCtr = 0.0f;
        }
        else {
            std::string str = "";
            //str += answerStrs[strIndex][0];
            curAnswerStrs.push_back(str);
            isPaused = true;
            timeCtr = 0.0f;
        }
    }
}

static void AnswerPauseLogic(const float dt)
{
    static const float PAUSE_TIME = 1.0f; // 1 second
    
    timeCtr += dt;
    if (timeCtr >= PAUSE_TIME) {
        timeCtr = 0;
        CurState = EndGameState::AuthorText;
    }
}

static std::string curAuthorText = "";
static void AuthorTextLogic(
    FreetypeFont& font,
    SoundEffect& typeWriterEffect,
    const float dt)
{
    static const std::string authorText =
        "-Mohammad Butterscotch, omniverse-plastikos.com";
    
    timeCtr += dt;
    if (timeCtr >= textSpeed) {
        curAuthorText += authorText[curAuthorText.size()];
        if (curAuthorText[curAuthorText.size()-1] != ' ') {
            typeWriterEffect.Play(EFFECT_CHANNEL, false);
        }
        while (timeCtr >= textSpeed) { timeCtr -= textSpeed; }
    }
    
    if (curAuthorText.size() == authorText.size()) {
        timeCtr = 0.0f;
        CurState = EndGameState::AuthorPause;
    }
}

static void AuthorPauseLogic(const float dt)
{
    static const float PAUSE_TIME = 4.0f; // 3 seconds
    
    timeCtr += dt;
    if (timeCtr >= PAUSE_TIME) {
        timeCtr = 0.0f;
        CurState = EndGameState::Done;
    }
}

void EndGameScreen()
{
    FreetypeFont questionFont;
    FreetypeFont answerFont;
    Sprite bgSprite;
    SoundEffect typeWriterEffect;
    Music vinylBgMusic;
    
    questionFont.Load(
        "assets/special-elite/SpecialElite.ttf",
        S_WIDTH, S_HEIGHT,
        0, 0, 0,
        20
    );
    answerFont.Load(
        "assets/special-elite/SpecialElite.ttf",
        S_WIDTH, S_HEIGHT,
        0, 0, 0,
        18 // smaller font size
    );
    bgSprite.Load("assets/papertexture_fullscreen.jpg", S_WIDTH, S_HEIGHT);
    typeWriterEffect.Load("assets/typewriter-sound.wav");
    vinylBgMusic.Load("assets/vinyleffect.wav");
    
    bool quit = false;
    vinylBgMusic.Play(true);
    while (!input.Quit() && !quit)
    {
        float dt = timer.Update();
        render.Clear();
        
        // LOGIC --------------------------------------------------
        switch (CurState)
        {
        case EndGameState::QuestionText:
            QuestionTextLogic(questionFont, typeWriterEffect, dt);
            break;
        case EndGameState::QuestionPause:
            QuestionPauseLogic(dt);
            break;
        case EndGameState::AnswerText:
            AnswerTextLogic(answerFont, typeWriterEffect, dt);
            break;
        case EndGameState::AnswerPause:
            AnswerPauseLogic(dt);
            break;
        case EndGameState::AuthorText:
            AuthorTextLogic(answerFont, typeWriterEffect, dt);
            break;
        case EndGameState::AuthorPause:
            AuthorPauseLogic(dt);
            break;
        case EndGameState::Done:
            quit = true;
            break;
        default:
            throw std::runtime_error("Unhandled Endgame screen state");
            break;
        }
        
        
        // DRAW ---------------------------------------------------
        bgSprite.Draw(render, 0, 0);
        // center of the screen
        int drawX = (S_WIDTH / 2) - 
            (curQuestionStr.size() * questionFont.GetWidth() / 2)
            + 30; // offset as width calculation not quite accurate
        questionFont.PrintString(
            render,
            curQuestionStr,
            drawX, 50
        );
        
        int drawY = 200;
        for (const std::string& str : curAnswerStrs) {
            drawX = 20;/*(S_WIDTH / 2) - 
            (str.size() * answerFont.GetWidth() / 2)
            + 30; // offset as width calculation not quite accurate*/
            
            answerFont.PrintString(render, str, drawX, drawY);
            drawY += answerFont.GetHeight() + 20;
        }
        
        if (curAuthorText.size() > 0) {
            drawX = 20;
            drawY = 500;
            answerFont.PrintString(render, curAuthorText, drawX, drawY);
        }
        
        input.Update();
        render.Update();
    }
    
    vinylBgMusic.Stop();
    
    // reset stuff in case this function is reached again
    curAnswerStrs.clear();
    curQuestionStr = "W";
    CurState = EndGameState::QuestionText;
    timeCtr = 0.0f;
    curAuthorText = "";
}
