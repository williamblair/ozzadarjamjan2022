#ifndef TEXT_DIALOGUE_H_INCLUDED
#define TEXT_DIALOGUE_H_INCLUDED

#include <FreetypeFont.h>

class TextDialogue
{
public:
    TextDialogue();
    ~TextDialogue();

    void SetFont(FreetypeFont* font) { mFont = font; }
    void SetX(int x) { mX = x; }
    void SetY(int y) { mY = y; }

    void SetText(const std::string& str) {
        mDone = false;
        mText = str;
        mCharIndex = 0;
        mTimeCounter = 0.0f;
        mCurText = "";
    }

    // time per character in seconds
    void SetTextSpeed(float speed) { mSpeed = speed; }
    bool Done() { return mDone; }

    // deltatime in seconds
    void Update(const float dt);

    void Draw(Renderer& render) { 
        mFont->PrintString(render, mCurText, mX, mY);
    }

private:
    FreetypeFont* mFont;
    int mX;
    int mY;
    int mW;
    std::string mText;
    std::string mCurText;
    float mSpeed;
    bool mDone;
    size_t mCharIndex;
    float mTimeCounter;
};

#endif // TEXT_DIALOGUE_H_INCLUDED

