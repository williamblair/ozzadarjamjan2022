#include <Renderer.h>

Renderer::Renderer() :
    mWindow(nullptr),
    mWidth(0),
    mHeight(0),
    mBGRed(0.0f),
    mBGGreen(0.0f),
    mBGBlue(0.0f)
{
}

Renderer::~Renderer()
{
    if (mWindow) {
        SDL_GL_DeleteContext(mContext);
        SDL_DestroyWindow(mWindow);
        SDL_Quit();
    }
}

void Renderer::Init(int width, int height, const char* title)
{
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) < 0) {
        std::cerr << __func__ << ": failed to init SDL: " << SDL_GetError() << std::endl;
        throw std::runtime_error("SDL_GetError()");
    }
    
    {
        int flags = MIX_INIT_OGG | MIX_INIT_MOD;
        if ((Mix_Init(flags) & flags) != flags) {
            std::cerr << __FILE__ << ": " << __LINE__ << ": "
                << "Failed to init Mix: " << Mix_GetError()
                << std::endl;
            throw std::runtime_error(Mix_GetError());
        }
    }
    
    mWindow = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
    if (!mWindow) {
        std::cerr << __func__ << ": failed to create window: " << SDL_GetError() << std::endl;
        throw std::runtime_error(SDL_GetError());
    }
    mWidth = width;
    mHeight = height;
    
    mPerspMat = Math::perspective(
        Math::ToRadians(60.0f),
        float(mWidth)/float(mHeight),
        0.1f,
        1000.0f
    );
    
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    
    mContext = SDL_GL_CreateContext(mWindow);
    SDL_GL_SetSwapInterval(1);
    
    // this should go in input....
    SDL_SetRelativeMouseMode(SDL_TRUE);
    
    glewInit();
    
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) == -1) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "Failed to open audio: " << Mix_GetError() << std::endl;
        throw std::runtime_error(Mix_GetError());
    }
}

void Renderer::Clear()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glClearColor(mBGRed, mBGGreen, mBGBlue, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::Update()
{
    SDL_GL_SwapWindow(mWindow);
}

