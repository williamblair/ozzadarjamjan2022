#include <Level1.h>

#include <Renderer.h>
#include <GameTimer.h>
#include <Input.h>
#include <Shader.h>
#include <Player.h>
#include <SoundEffect.h>
#include <AssimpModel.h>
#include <Sprite.h>
#include <FreetypeFont.h>
#include <TextDialogue.h>

// Globals from main.cpp
extern int S_WIDTH;
extern int S_HEIGHT;
extern Renderer render;
extern GameTimer timer;
extern Input input;

static bool PlayerNextToDoor(Player& player, TileMap& map)
{
    AABB doorAabb;
    const float tileSize = 5.0f;
    const float tileOffs = tileSize / 2.0f;
    doorAabb.pos = map.GetTilePosition(1,8);
    doorAabb.min = Math::Vec3FromVals(-tileOffs, -tileOffs, -tileOffs);
    doorAabb.max = Math::Vec3FromVals(
        tileOffs,
        tileOffs,
        tileOffs + tileSize); // include the next tile downwards also
    return player.GetAabb().Intersects(doorAabb);
}

static bool PlayerNextToHeart(Player& player, AssimpModel& heart)
{
    const float tileSize = 5.0f;
    const float tileOffs = tileSize / 2.0f;
    // AABB at the model position with the size of a full tile
    AABB modelAabb = heart.GetCollider();
    modelAabb.min = Math::Vec3FromVals(-tileOffs, -tileOffs, -tileOffs);
    modelAabb.max = Math::Vec3FromVals(tileOffs, tileOffs, tileOffs);
    return player.GetAabb().Intersects(modelAabb);
}

static bool PlayerNextToLung(Player& player, AssimpModel& lung)
{
    const float tileSize = 5.0f;
    const float tileOffs = tileSize / 2.0f;
    // AABB at the model position with the size of a full tile
    AABB modelAabb = lung.GetCollider();
    modelAabb.min = Math::Vec3FromVals(-tileOffs, -tileOffs, -tileOffs);
    modelAabb.max = Math::Vec3FromVals(tileOffs, tileOffs, tileOffs);
    return player.GetAabb().Intersects(modelAabb);
}

static inline bool MouseOverlaps15(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 200 &&
        input.MouseX() <= (200 + strlen("15")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 100) &&
        input.MouseY() >= ((S_HEIGHT - 100) - fontHeight));
}

static inline bool MouseOverlaps16(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 575 &&
        input.MouseX() <= (575 + strlen("16")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 100) &&
        input.MouseY() >= ((S_HEIGHT - 100) - fontHeight));
}

static inline bool MouseOverlaps17(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 200 &&
        input.MouseX() <= (200 + strlen("17")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 50) &&
        input.MouseY() >= ((S_HEIGHT - 50) - fontHeight));
}

static inline bool MouseOverlaps18(int fontWidth, int fontHeight)
{
    return (input.MouseX() >= 500 &&
        input.MouseX() <= (575 + strlen("18")*fontWidth) &&
        input.MouseY() <= (S_HEIGHT - 50) &&
        input.MouseY() >= ((S_HEIGHT - 50) - fontHeight));
}

static void DrawQuestionChoices(
    FreetypeFont& font,
    FreetypeFont& fontColored)
{
    FreetypeFont* f = MouseOverlaps15(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "15", 200, S_HEIGHT - 100);
    
    f = MouseOverlaps16(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "16", 575, S_HEIGHT - 100);
    
    f = MouseOverlaps17(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "17", 200, S_HEIGHT - 50);
    
    f = MouseOverlaps18(
        font.GetWidth(), font.GetHeight()) ? 
        &fontColored : 
        &font;
    f->PrintString(render, "18", 575, S_HEIGHT - 50);
}

bool Level1()
{
    AssimpModel lungModel;
    AssimpModel heartModel;
    Texture heartTexture;
    Sprite dialogueBg;
    Sprite info1Spr;
    Sprite info2Spr;
    Sprite* curInfoSpr = nullptr;
    Sprite scalpelSprite;
    TileMap map;
    Shader tileMapShader;
    Shader* modelShader;
    Player player;
    SoundEffect walkEffect;
    SoundEffect lockedEffect; // door locked
    SoundEffect openEffect; // door opened
    SoundEffect unlockedEffect; // door unlocked
    SoundEffect paperFlipEffect;
    FreetypeFont dialogueFont;
    FreetypeFont dialogueFontColored;
    TextDialogue textDialogue;
    bool runningTextDialogue = false;
    bool showingInfoText = false;
    
    int itemCount = 0;
    const int requiredItems = 2;
    bool heartCollected = false;
    bool lungCollected = false;
    
    lungModel.Load("assets/uploads_files_2015339_Human_Lung_Low_Poly.obj");
    {
        const float tileSize = 5.0f;
        Vec3 pos = map.GetTilePosition(8, 3);
        pos.y -= tileSize / 2.0f;
        lungModel.SetPosition(pos);
        lungModel.SetScale(Vec3(0.5f, 0.5f, 0.5f));
        lungModel.SetRotation(
            Math::ToRadians(90.0f),
            Math::Vec3FromVals(0.0f,1.0f,0.0f)
        );
    }
    heartModel.Load("assets/Heart.obj"); // TODO - textures
    {
        const float tileSize = 5.0f;
        Vec3 pos = map.GetTilePosition(4, 4);
        pos.y -= tileSize / 2.0f;
        heartModel.SetPosition(pos);
        heartModel.SetScale(Vec3(0.05f, 0.05f, 0.05f));
    }
    heartTexture.Load("assets/Heart.png");
    heartModel.SetTexture(&heartTexture);
    
    tileMapShader.Init("Shaders/TexturedVert.glsl", "Shaders/TexturedFrag.glsl");
    modelShader = &tileMapShader; // both used texturedvert/frag
    
    map.Load("assets/level1.txt");
    map.AddTexture(0, "assets/rustground.jpg");
    map.AddTexture(1, "assets/bloodytilewall.png");
    map.AddTexture(2, "assets/rustydoorleft.jpg");
    map.AddTexture(3, "assets/rustydoorright.jpg");
    map.AddCeilTexture("assets/rustygrate.jpg");
    
    dialogueBg.Load("assets/dialoguebg.jpg", S_WIDTH, S_HEIGHT);
    info1Spr.Load("assets/jessica1.png", S_WIDTH, S_HEIGHT);
    info2Spr.Load("assets/jessica2.png", S_WIDTH, S_HEIGHT);
    scalpelSprite.Load("assets/scalpel.png", S_WIDTH, S_HEIGHT);
    
    walkEffect.Load("assets/jute-dh-steps/stepstone_1.wav");
    lockedEffect.Load("assets/doorlocked.wav");
    openEffect.Load("assets/dooropen.ogg");
    unlockedEffect.Load("assets/doorunlock.mp3");
    paperFlipEffect.Load("assets/paperflipeffect.mp3");
    
    dialogueFont.Load(
        "assets/Alice-Regular.ttf",
        S_WIDTH, S_HEIGHT,
        255,255,255,
        18
    );
    dialogueFontColored.Load(
        "assets/Alice-Regular.ttf",
        S_WIDTH, S_HEIGHT,
        255,255,0,
        18
    );
    
    player.SetWalkEffect(&walkEffect);
    {
        const float tileSize = 5.0f;
        Vec3 tileCenter = map.GetTilePosition(1, 1);
        tileCenter.y -= tileSize / 2.0f;
        player.SetPosition(tileCenter);
    }
    player.SetYaw(270.0f);
    
    player.AddColliders(map.GetColliders());
    player.AddCollider(heartModel.GetCollider());
    player.AddCollider(lungModel.GetCollider());
    
    bool quit = false;

    // Instructions dialogue
    std::string instructionStrs[4] = {
        "Mouse Click: Confirm/Interact",
        "W/A/S/D: Move",
        "Q/E: Rotate",
        "Pressing Escape will exit the program"
    };
    size_t instStrIndex = 0;
    textDialogue.SetText(instructionStrs[0]);
    textDialogue.SetTextSpeed(0.025f);
    textDialogue.SetX(150+10);
    textDialogue.SetY(S_HEIGHT - dialogueBg.GetHeight() + 50);
    textDialogue.SetFont(&dialogueFont);
    while (!quit && !input.Quit())
    {
        float dt = timer.Update();
        
        render.Clear();
        
        // LOGIC -----------------------------------------------------
        textDialogue.Update(dt);
        if (textDialogue.Done() && input.MouseClicked()) {
            ++instStrIndex;
            if (instStrIndex >= 4) {
                quit = true;
            }
            else {
                textDialogue.SetText(instructionStrs[instStrIndex]);
            }
        }

        // DRAWING ---------------------------------------------------
        tileMapShader.Use();
        map.Draw(render, player.GetViewMat(), tileMapShader);

        dialogueBg.Draw(
            render, 
            150, S_HEIGHT - dialogueBg.GetHeight() - 20
        );
        textDialogue.Draw(render);

        player.Update();
        input.Update();
        render.Update();
    }

    // Main gameplay loop
    quit = false;
    while (!quit && !input.Quit())
    {
        float dt = timer.Update();
        
        render.Clear();
        
        // LOGIC -----------------------------------------------------
        bool playerNextToDoor = false;
        bool playerNextToHeart = false;
        bool playerNextToLung = false;
        if (!runningTextDialogue && !showingInfoText) {
            playerNextToDoor = PlayerNextToDoor(player, map);
            if (!heartCollected) {
                playerNextToHeart = PlayerNextToHeart(player, heartModel);
            }
            if (!lungCollected) {
                playerNextToLung = PlayerNextToLung(player, lungModel);
            }
            player.Move(input, dt);
            
            if (playerNextToDoor) {
                if (input.MouseClicked()) {
                    if (itemCount == requiredItems) {
                        openEffect.Play();
                        quit = true;
                    }
                    else {
                        lockedEffect.Play();
                        runningTextDialogue = true;
                        textDialogue.SetText("The door seems to be locked.");
                        textDialogue.SetTextSpeed(0.025f);
                        textDialogue.SetX(150+10);
                        textDialogue.SetY(S_HEIGHT - dialogueBg.GetHeight() + 50);
                        textDialogue.SetFont(&dialogueFont);
                    }
                }
            }
            
            else if (playerNextToHeart) {
                if (input.MouseClicked()) {
                    paperFlipEffect.Play();
                    if (!heartCollected) {
                        heartCollected = true;
                        ++itemCount;
                        player.RemoveCollider(heartModel.GetCollider());
                        showingInfoText = true;
                        if (itemCount == 1) { curInfoSpr = &info1Spr; }
                        else                { curInfoSpr = &info2Spr; }
                    }
                }
            }
            else if (playerNextToLung) {
                if (input.MouseClicked()) {
                    paperFlipEffect.Play();
                    if (!lungCollected) {
                        lungCollected = true;
                        ++itemCount;
                        player.RemoveCollider(lungModel.GetCollider());
                        showingInfoText = true;
                        if (itemCount == 1) { curInfoSpr = &info1Spr; }
                        else                { curInfoSpr = &info2Spr; }
                    }
                }
            }
        }
        else if (runningTextDialogue) {
            player.StopWalkEffect();
            textDialogue.Update(dt);
            if (textDialogue.Done() &&
                input.MouseClicked())
            {
                runningTextDialogue = false;
            }
        }
        // showingInfoText == true
        else {
            player.StopWalkEffect();
            if (input.MouseClicked()) {
                showingInfoText = false;
                if (itemCount == 2) { unlockedEffect.Play(); }
            }
        }
        
        // DRAWING ---------------------------------------------------
        tileMapShader.Use();
        map.Draw(render, player.GetViewMat(), tileMapShader);
        
        if (!heartCollected) {
            modelShader->Use();
            heartModel.Draw(render, player.GetViewMat(), *modelShader);
        }
        if (!lungCollected) {
            modelShader->Use();
            lungModel.Draw(render, player.GetViewMat(), *modelShader);
        }
        
        if (runningTextDialogue) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            textDialogue.Draw(render);
        }
        else if (showingInfoText) {
            curInfoSpr->Draw(
                render,
                50, 50
            );
        }
        else if (playerNextToDoor) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            dialogueFont.PrintString(
                render,
                "Mouse Click: Open",
                150+10, S_HEIGHT - dialogueBg.GetHeight() + 50
            );
        }
        else if (playerNextToHeart || playerNextToLung) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            dialogueFont.PrintString(
                render,
                "Mouse Click: Inspect",
                150+10, S_HEIGHT - dialogueBg.GetHeight() + 50
            );
        }

        player.Update();
        input.Update();
        render.Update();
    }
    
    if (input.Quit()) { return false; }
    
    player.StopWalkEffect();
    quit = false;
    bool passed = false;
    runningTextDialogue = true;
    textDialogue.SetText("How old was Jesica Santillan when she passed?");
    textDialogue.SetTextSpeed(0.025f);
    textDialogue.SetX(150+10);
    textDialogue.SetY(S_HEIGHT - dialogueBg.GetHeight() + 50);
    textDialogue.SetFont(&dialogueFont);
    while (!input.Quit() && !quit)
    {
        float dt = timer.Update();
        render.Clear();
        
        // LOGIC --------------------------------------------------
        if (runningTextDialogue) {
            textDialogue.Update(dt);
            if (textDialogue.Done() &&
                input.MouseClicked())
            {
                runningTextDialogue = false;
            }
        }
        // User input select
        else {
            if (input.MouseClicked()) {
                if (MouseOverlaps15(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    passed = false;
                    quit = true;
                }
                else if (MouseOverlaps16(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    passed = false;
                    quit = true;
                }
                else if (MouseOverlaps17(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    passed = true;
                    quit = true;
                }
                else if (MouseOverlaps18(dialogueFont.GetWidth(), dialogueFont.GetHeight())) {
                    passed = false;
                    quit = true;
                }
            }
        }
        
        // DRAW ---------------------------------------------------
        tileMapShader.Use();
        map.Draw(render, player.GetViewMat(), tileMapShader);
        if (runningTextDialogue) {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            textDialogue.Draw(render);
        }
        // user question guess input
        else {
            dialogueBg.Draw(
                render, 
                150, S_HEIGHT - dialogueBg.GetHeight() - 20
            );
            DrawQuestionChoices(dialogueFont, dialogueFontColored);
            scalpelSprite.Draw(render, input.MouseX(), input.MouseY());
        }
        
        player.Update();
        input.Update();
        render.Update();
    }
    
    return passed;
}

