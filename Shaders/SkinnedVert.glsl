#version 330 core

layout (location=0) in vec3 aPos;
layout (location=1) in vec3 aNormal;
layout (location=2) in uvec4 aSkinBones; // bone indices
layout (location=3) in vec4 aSkinWeights; // vertex skin weights
layout (location=4) in vec2 aTexCoord;

// model, view, projection matrix
uniform mat4 uTransform;

// skinning matrices
uniform mat4 uMatrixPalette[96]; // max bones = 96

// tex coordinate for the frag shader to use
out vec2 fragTexCoord;

void main()
{
    vec4 pos = vec4(aPos, 1.0);
    
    // skin the position
    vec4 skinnedPos = (uMatrixPalette[aSkinBones.x] * pos) * aSkinWeights.x;
    skinnedPos += (uMatrixPalette[aSkinBones.y] * pos) * aSkinWeights.y;
    skinnedPos += (uMatrixPalette[aSkinBones.z] * pos) * aSkinWeights.z;
    skinnedPos += (uMatrixPalette[aSkinBones.w] * pos) * aSkinWeights.w;
    
    // transform position
    skinnedPos = uTransform * vec4(skinnedPos.xzy, 1.0); // swap y and z (because of model coord system)
    gl_Position = skinnedPos;
    
    // TODO
    //vec4 skinnedNormal = vec4(aNormal, 0.0);
    //skinnedNormal = (skinnedNormal * uMatrixPalette[aSkinBones.x]) * aSkinWeights.x +
    //    (skinnedNormal * uMatrixPalette[aSkinBones.y]) * aSkinWeights.y +
    //    (skinnedNormal * uMatrixPalette[aSkinBones.z]) * aSkinWeights.z +
    //    (skinnedNormal * uMatrixPalette[aSkinBones.w]) * aSkinWeights.w;
    //fragNormal = (skinnedNormal * uTransform).xyz;
    
    fragTexCoord = vec2(aTexCoord.x, aTexCoord.y);
}
