#version 330 core

layout (location=0) in vec2 aPos;
layout (location=1) in vec2 aTexCoord;

uniform mat4 uTransform;

out vec2 fragTexCoord;

void main()
{
    vec4 pos = vec4(aPos.x, aPos.y, 0.0, 1.0);
    pos = uTransform * pos;
    gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);
    fragTexCoord = aTexCoord;
}

