#version 330 core

in vec2 fragTexCoord;
out vec4 FragColor;

uniform sampler2D uTexture;

void main()
{
    FragColor = texture2D(uTexture, fragTexCoord);
    // don't draw if transparent
    if (FragColor.a == 0.0) {
        discard;
    }
}

