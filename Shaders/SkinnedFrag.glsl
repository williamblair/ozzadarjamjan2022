#version 330 core

in vec2 fragTexCoord;

uniform sampler2D uTexSamp;

out vec4 FragColor;

void main()
{
    //FragColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
    FragColor = texture(uTexSamp, fragTexCoord);
}
