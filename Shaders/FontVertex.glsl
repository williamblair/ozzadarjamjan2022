#version 330 core

layout (location=0) in vec2 aPosition;
layout (location=1) in vec2 aTexCoord0;

uniform vec2 uTranslate;

out vec2 vTexCoord0;

void main()
{
    vTexCoord0 = aTexCoord0;
    gl_Position = vec4(aPosition + uTranslate,
                       0.0,
                       1.0);
}
