#include <Camera.h>

FPSCamera::FPSCamera()
{
    mViewMat = Math::lookAt(
        mMoveComponent.position,
        mMoveComponent.target,
        mMoveComponent.up
    );
}

FPSCamera::~FPSCamera()
{
}

void FPSCamera::Update()
{
    mMoveComponent.Update();
    
    // Update view matrix
    mViewMat = Math::lookAt(
        mMoveComponent.position,
        mMoveComponent.target,
        mMoveComponent.up
    );
}

OrbitCamera::OrbitCamera() :
    mMoveComponent(nullptr),
    mPosition(Vec3(10.0f, 0.0f, 0.0f)),
    mDistance(10.0f),
    mYaw(0.0f),
    mPitch(0.0f)
{
    Update();
}

void OrbitCamera::Update()
{
    Vec3 targetPos(0.0f, 0.0f, 0.0f);
    if (mMoveComponent) {
        targetPos = mMoveComponent->position;
    }
    
    // calculate right axis
    Vec3 right = Math::angleAxis(Math::ToRadians(mYaw), Vec3(0.0f, 1.0f, 0.0f)) * // yaw about y axis
        Vec3(1.0f, 0.0f, 0.0f);
    right = Math::Normalize(right);
    
    // calculate up axis
    Vec3 up = Math::angleAxis(Math::ToRadians(mPitch), right) * // pitch about rotated x axis
        Vec3(0.0f, 1.0f, 0.0f);
    up = Math::Normalize(up);
    
    // calculate forward axis
    Vec3 forward = Math::Cross(up, right);
    forward = Math::Normalize(forward);
    
    // set our location backwards from the target
    mPosition = targetPos - forward * mDistance;
    
    // update view matrix
    mViewMat = Math::lookAt(mPosition, targetPos, Vec3(0.0f, 1.0f, 0.0f));
}

ThirdPersonCamera::ThirdPersonCamera() :
    mDistance(10.0f)
{
    Update();
}

void ThirdPersonCamera::Update()
{
    mMoveComponent.Update();
    
    Vec3 position = mMoveComponent.position - mMoveComponent.forward * mDistance;
    mViewMat = Math::lookAt(
        position, // eye position
        mMoveComponent.position, // target position
        Vec3(0.0f, 1.0f, 0.0f) // up
    );
}
