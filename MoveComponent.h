#ifndef MOVE_COMPONENT_H_INCLUDED
#define MOVE_COMPONENT_H_INCLUDED

#include <Math.h>

struct MoveComponent
{
    Vec3 position;
    Vec3 target;
    Vec3 up;
    Vec3 forward;
    Vec3 right;
    
    float pitch;
    float yaw;
    
    MoveComponent();
    
    void Update();
    
    void MoveForward(const float amount) {
        position += forward * amount;
    }
    void MoveRight(const float amount) {
        position += right * amount;
    }
    
    void AddPitch(const float amount) {
        pitch += amount;
        pitch = Math::Clamp(pitch, -80.0f, 80.0f);
    }
    void AddYaw(const float amount) {
        yaw += amount;
        while (yaw < 0.0f) { yaw += 360.0f; }
        while (yaw > 360.0f) { yaw -= 360.0f; }
    }
};

#endif // MOVE_COMPONENT_H_INCLUDED
