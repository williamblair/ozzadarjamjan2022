#ifndef ANIMATION_H_INCLUDED
#define ANIMATION_H_INCLUDED

#include <vector>
#include <string>

#include <BoneTransform.h>

class Animation
{
public:
    
    // Load from file
    bool Load(const std::string& fileName);
    
    // Getters
    size_t GetNumBones() const { return mNumBones; }
    size_t GetNumFrames() const { return mNumFrames; }
    float GetDuration() const { return mDuration; }
    float GetFrameDuration() const { return mFrameDuration; }
    
    // Fills the provided vector with the global (current) pose matrices for
    // each bone at the specified time in the animation. It is expected that the
    // time is >= 0.0f and <= mDuration
    void GetGlobalPoseAtTime(
        std::vector<Mat4>& outPoses,
        const class Skeleton* inSkeleton,
        float inTime) const;
    
private:
    
    // number of bones for the animation
    size_t mNumBones;
    // number of frames in the animation
    size_t mNumFrames;
    // Duration of the animation in seconds
    float mDuration;
    // Duration of each frame in the animation
    float mFrameDuration;
    // Transform information for each frame on the track
    // Each index in the outer vector is a bone, inner vector is a frame
    std::vector<std::vector<BoneTransform>> mTracks;
};

#endif // ANIMATION_H_INCLUDED
