#ifndef AXIS_ALIGNED_BOUNDING_BOX_H_INCLUDED
#define AXIS_ALIGNED_BOUNDING_BOX_H_INCLUDED

#include <Math.h>

// Axis Aligned Bounding Box
struct AABB
{
    Vec3 min; // size minimums
    Vec3 max; // size maximums
    Vec3 pos; // position
    
    inline bool Intersects(const AABB& other) {
        if (min.x + pos.x > other.max.x + other.pos.x ||
            max.x + pos.x < other.min.x + other.pos.x ||
            min.y + pos.y > other.max.y + other.pos.y ||
            max.y + pos.y < other.min.y + other.pos.y ||
            min.z + pos.z > other.max.z + other.pos.z ||
            max.z + pos.z < other.min.z + other.pos.z)
        {
            return false;
        }
        return true;
    }
};

#endif // AXIS_ALIGNED_BOUNDING_BOX_H_INCLUDED
