#include <Skeleton.h>
#include <MatrixPalette.h>

#include <rapidjson/document.h>

#include <fstream>
#include <sstream>

bool Skeleton::Load(const std::string& fileName)
{
    std::ifstream file(fileName);
    if (!file.is_open()) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "File not found: Skeleton " << fileName << std::endl;
        return false;
    }
    
    std::stringstream fileStream;
    fileStream << file.rdbuf();
    std::string contents = fileStream.str();
    rapidjson::StringStream jsonStr(contents.c_str());
    rapidjson::Document doc;
    doc.ParseStream(jsonStr);
    
    if (!doc.IsObject())
    {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "Skeleton is not valid json: " << fileName << std::endl;
        return false;
    }
    
    int ver = doc["version"].GetInt();
    
    // check metadata
    if (ver != 1)
    {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "Skeleton unknown format version: " << fileName << std::endl;
        return false;
    }
    
    const rapidjson::Value& bonecount = doc["bonecount"];
    if (!bonecount.IsUint())
    {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "Skeleton doesn't have a bone count: " << fileName << std::endl;
        return false;
    }
    
    size_t count = bonecount.GetUint();
    if (count > MAX_SKELETON_BONES) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "Skeleton exceeds max bone count: " << fileName << std::endl;
        return false;
    }
    mBones.reserve(count);
    
    const rapidjson::Value& bones = doc["bones"];
    if (!bones.IsArray()) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "Skeleton doesn't have bone array: " << fileName << std::endl;
        return false;
    }
    if (bones.Size() != count) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "skeleton bone count mismatch: " << fileName << std::endl;
        return false;
    }
    
    Bone temp;
    
    for (rapidjson::SizeType i = 0; i < count; ++i)
    {
        if (!bones[i].IsObject()) {
            std::cerr << __FILE__ << ": " << __LINE__ << ": "
                << "Skeleton bone is invalid: " << i << std::endl;
            return false;
        }
        
        const rapidjson::Value& name = bones[i]["name"];
        temp.mName = name.GetString();
        
        const rapidjson::Value& parent = bones[i]["parent"];
        temp.mParent = parent.GetInt();
        
        const rapidjson::Value& bindpose = bones[i]["bindpose"];
        if (!bindpose.IsObject()) {
            std::cerr << __FILE__ << ": " << __LINE__ << ": "
                << "Skeleton bindpose not object: " << i << std::endl;
            return false;
        }
        
        const rapidjson::Value& rot = bindpose["rot"];
        const rapidjson::Value& trans = bindpose["trans"];
        
        if (!rot.IsArray() || !trans.IsArray()) {
            std::cerr << __FILE__ << ": " << __LINE__ << ": "
                << "Skeleton rot or trans not array: " << i << std::endl;
            return false;
        }
        
        temp.mLocalBindPose.rotation = Math::QuatFromVals(
            rot[0].GetDouble(),
            rot[1].GetDouble(),
            rot[2].GetDouble(),
            rot[3].GetDouble()
        );
        /*temp.mLocalBindPose.rotation.x = rot[0].GetDouble();
        temp.mLocalBindPose.rotation.y = rot[1].GetDouble();
        temp.mLocalBindPose.rotation.z = rot[2].GetDouble();
        temp.mLocalBindPose.rotation.w = rot[3].GetDouble();*/
        
        temp.mLocalBindPose.translation = Math::Vec3FromVals(
            trans[0].GetDouble(),
            trans[1].GetDouble(),
            trans[2].GetDouble()
        );
        
        mBones.emplace_back(temp);
    }
    
    // Now that we have the bones...
    ComputeGlobalInvBindPose();
    
    // Success!
    return true;
}

void Skeleton::ComputeGlobalInvBindPose()
{
    // Resize to number of bones
    mGlobalInvBindPoses.resize(GetNumBones());
    // Init each matrix to identity
    for (Mat4& mat : mGlobalInvBindPoses)
    {
        mat = Math::identity();
    }
    
    // Step 1: compute global bind pose for each bone
    
    // The global bind pose for root is just the local bind pose
    mGlobalInvBindPoses[0] = mBones[0].mLocalBindPose.ToMatrix();
    
    // Each remaining bone's global bind pose is its local pose
    // multiplied by the parent's global bind pose
    for (size_t i = 1; i < mGlobalInvBindPoses.size(); ++i)
    {
        Mat4 localMat = mBones[i].mLocalBindPose.ToMatrix();
        mGlobalInvBindPoses[i] = 
            //localMat * mGlobalInvBindPoses[mBones[i].mParent];
            mGlobalInvBindPoses[mBones[i].mParent] * localMat;
    }
    
    // Step 2: invert
    for (size_t i = 0; i < mGlobalInvBindPoses.size(); ++i)
    {
        mGlobalInvBindPoses[i] = Math::inverse(mGlobalInvBindPoses[i]);
    }
}
