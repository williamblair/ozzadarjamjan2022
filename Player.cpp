#include <Player.h>
#include <iostream>

Player::Player() :
    mPosition(Vec3(0.0f, 0.0f, 0.0f)),
    mYaw(0.0f),
    mWalkEffect(nullptr),
    mPlayingWalkEffect(false)
{
    mAabb.pos = mPosition;
    SetSize(2.5f);
}

Player::~Player()
{
}

void Player::Move(Input& input, const float dt)
{
    MoveComponent& mc = mCamera.GetMoveComponent();
    
    // attempting tank controls...
    const float moveSpeed = 10.0f;
    const float yawSpeed = 50.0f;
    const float walkUpSpeed = Math::TwoPi; // Two Pi radians per second
    bool walking = false;
    if (input.YawRotPositive()) { 
        mc.AddYaw(yawSpeed * dt);
        mWalkingUpTime = 0.0f;
    }
    else if (input.YawRotNegative()) { 
        mc.AddYaw(-yawSpeed * dt);
        mWalkingUpTime = 0.0f;
    }
    else if (input.Forward()) { 
        mc.MoveForward(moveSpeed * dt);
        mWalkingUpTime += walkUpSpeed * dt;
        walking = true;
        mAabb.pos = mc.position;
        mAabb.pos.y -= mSize;
        if (PlayerCollidesAtCurPos()) {
            mc.MoveForward(-moveSpeed * dt);
            mAabb.pos = mc.position;
            mAabb.pos.y -= mSize;
        }
    }
    else if (input.Backward()) {
        mc.MoveForward(-moveSpeed * dt);
        mWalkingUpTime += walkUpSpeed * dt;
        walking = true;
        mAabb.pos = mc.position;
        mAabb.pos.y -= mSize;
        if (PlayerCollidesAtCurPos()) {
            mc.MoveForward(moveSpeed * dt);
            mAabb.pos = mc.position;
            mAabb.pos.y -= mSize;
        }
    }
    else if (input.Left()) {
        mc.MoveRight(-moveSpeed * dt);
        mWalkingUpTime += walkUpSpeed * dt;
        walking = true;
        mAabb.pos = mc.position;
        mAabb.pos.y -= mSize;
        if (PlayerCollidesAtCurPos()) {
            mc.MoveRight(moveSpeed * dt);
            mAabb.pos = mc.position;
            mAabb.pos.y -= mSize;
        }
    }
    else if (input.Right()) {
        mc.MoveRight(moveSpeed * dt);
        mWalkingUpTime += walkUpSpeed * dt;
        walking = true;
        mAabb.pos = mc.position;
        mAabb.pos.y -= mSize;
        if (PlayerCollidesAtCurPos()) {
            mc.MoveRight(-moveSpeed * dt);
            mAabb.pos = mc.position;
            mAabb.pos.y -= mSize;
        }
    }
    else {
        mWalkingUpTime = 0.0f;
    }
    
    while (mWalkingUpTime > Math::Pi) { mWalkingUpTime -= Math::Pi; }

    if (!walking && mPlayingWalkEffect) {
        mWalkEffect->Stop(sWalkEffectChannel);
        mPlayingWalkEffect = false;
    }
    else if (walking && !mPlayingWalkEffect) {
        mWalkEffect->Play(sWalkEffectChannel, true);
        mPlayingWalkEffect = true;
    }
}

void Player::Update()
{
    mCamera.Update();
    mPosition = mCamera.GetPosition();
    mPosition.y -= mSize;
    const float walkUpScale = 0.35f;
    const float walkYOffs = Math::Sin(mWalkingUpTime) * walkUpScale;
    mPosition.y += walkYOffs;
    // negative y because we're making a view matrix, not a model matrix
    mViewMat =
        mCamera.GetViewMat() *
        Math::translate(0.0f, -walkYOffs, 0.0f);
    mAabb.pos = mPosition;
}

