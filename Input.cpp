#include <Input.h>

#include <iostream>

Input::Input() :
    mForward(false),
    mBackward(false),
    mLeft(false),
    mRight(false),
    mYawPos(false),
    mYawNeg(false),
    mMouseMoveX(0),
    mMouseMoveY(0),
    mMouseX(0),
    mMouseY(0),
    mMouseClicked(false),
    mMouseHeld(false)
{
}
Input::~Input()
{
}

void Input::Update()
{
    SDL_Event e;
    mMouseMoveX = 0;
    mMouseMoveY = 0;
    mMouseClicked = false;
    while (SDL_PollEvent(&e))
    {
        switch (e.type)
        {
        case SDL_QUIT:
            mQuit = true;
            break;
        case SDL_KEYDOWN:
            switch (e.key.keysym.sym)
            {
            case SDLK_w: mForward = true; break;
            case SDLK_a: mLeft = true; break;
            case SDLK_s: mBackward = true; break;
            case SDLK_d: mRight = true; break;
            case SDLK_q: mYawPos = true; break;
            case SDLK_e: mYawNeg = true; break;
            default:
                break;
            }
            break;
        case SDL_KEYUP:
            switch (e.key.keysym.sym)
            {
            case SDLK_w: mForward = false; break;
            case SDLK_a: mLeft = false; break;
            case SDLK_s: mBackward = false; break;
            case SDLK_d: mRight = false; break;
            case SDLK_q: mYawPos = false; break;
            case SDLK_e: mYawNeg = false; break;
            case SDLK_ESCAPE: mQuit = true; break;
            default:
                break;
            }
            break;
        case SDL_MOUSEMOTION:
            mMouseMoveX = e.motion.xrel;
            mMouseMoveY = e.motion.yrel;
            mMouseX = e.motion.x;
            mMouseY = e.motion.y;
            //std::cout << "Mouse x, y: " << mMouseX << " " << mMouseY << std::endl;
            break;
        case SDL_MOUSEBUTTONDOWN:
            switch (e.button.button)
            {
            case SDL_BUTTON_LEFT:
                //std::cout << "Left mouse button down" << std::endl;
                mMouseHeld = true;
                break;
            default:
                break;
            }
            break;
        case SDL_MOUSEBUTTONUP:
            switch (e.button.button)
            {
            case SDL_BUTTON_LEFT:
                //std::cout << "Left mouse button up: " 
                //    << mMouseX << " " << mMouseY << std::endl;
                mMouseClicked = true;
                mMouseHeld = false;
                break;
            default:
                break;
            }
            break;
        default:
            break;
        }
    }
}
