#include <NextLevelScreen.h>

#include <string>

#include <Renderer.h>
#include <GameTimer.h>
#include <Input.h>
#include <Music.h>
#include <SoundEffect.h>
#include <FreetypeFont.h>
#include <Sprite.h>

// Globals used by other gameplay functions
extern int S_WIDTH;
extern int S_HEIGHT;
extern Renderer render;
extern GameTimer timer;
extern Input input;
extern Music BgMusic;

static bool assetsLoaded = false;
static SoundEffect levelEffect;
static FreetypeFont font;
static Sprite bgSprite;

void NextLevelScreen(const int level)
{
    if (!assetsLoaded)
    {
        levelEffect.Load("assets/leveleffect.wav");
        font.Load(
            "assets/ImpactLabelReversed.ttf",
            S_WIDTH, S_HEIGHT,
            255, 0, 0,
            28
        );
        bgSprite.Load("assets/operatingroombj.jpg", S_WIDTH, S_HEIGHT);
        assetsLoaded = true;
    }
    
    float timeCtr = 0.0f;
    const float textSpeed = 0.1f;
    std::string str = "[ L E V E L   ";
    str += std::to_string(level);
    str += " ]";
    std::string curStr = "[";
    
    const float blinkSpeed = 0.5f;
    bool blinkShow = false;
    std::string confirmStr = "Mouse Click: Continue";
    
    bool animating = true;
    bool quit = false;
    levelEffect.Play();
    while (!quit && !input.Quit())
    {
        float dt = timer.Update();
        render.Clear();
        
        // LOGIC ----------------------------------------------------
        if (animating) {
            timeCtr += dt;
            if (timeCtr >= textSpeed) {
                curStr += str[curStr.size()];
                while (timeCtr >= textSpeed) { timeCtr -= textSpeed; }
                if (curStr.size() == str.size()) {
                    animating = false;
                    timeCtr = 0.0f;
                }
            }
        }
        else {
            timeCtr += dt;
            if (timeCtr >= blinkSpeed) {
                blinkShow = !blinkShow;
                while (timeCtr >= blinkSpeed) { timeCtr -= blinkSpeed; }
            }
            if (input.MouseClicked()) {
                quit = true;
            }
        }
        
        // DRAWING --------------------------------------------------
        bgSprite.Draw(render, 0, 0);
        
        int drawX = (S_WIDTH/2) - (curStr.size()*font.GetWidth())/2
            - 70; // inaccurate calculation offset
        int drawY = 200;
        font.PrintString(render, curStr, drawX, drawY);
        
        if (blinkShow) {
            drawX = (S_WIDTH/2) - (confirmStr.size()*font.GetWidth())/2
                - 70; // inaccurate calculation offset
            drawY = 500;
            font.PrintString(render, confirmStr, drawX, drawY);
        }
        
        render.Update();
        input.Update();
    }
}
