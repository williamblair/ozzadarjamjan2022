#include <Texture.h>

Texture::Texture() :
    mWidth(0),
    mHeight(0),
    mNumChannels(0),
    mTexId(0)
{
}

Texture::~Texture()
{
    if (mTexId != 0) {
        glDeleteTextures(1, &mTexId);
    }
}

bool Texture::Load(const std::string& fileName)
{
    stbi_set_flip_vertically_on_load(true);
    unsigned char* data = stbi_load(
        fileName.c_str(),
        &mWidth,
        &mHeight,
        &mNumChannels,
        0
    );
    
    if (!data) {
        std::cerr << __FILE__ << ": " << __LINE__ << ": "
            << "failed to load texture: " << fileName << std::endl;
        return false;
    }
    
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &mTexId);
    std::cout << "Tex name: " << fileName << ": Gen tex id: " << mTexId << std::endl;
    glBindTexture(GL_TEXTURE_2D, mTexId);
    glTexImage2D(
        GL_TEXTURE_2D,
        0, // mipmap level; 0 = default = OpenGL chooses
        mNumChannels <= 3 ? GL_RGB : GL_RGBA, // OpenGL internal storage format
        mWidth,
        mHeight,
        0, // always 0
        mNumChannels <= 3 ? GL_RGB : GL_RGBA, // input format
        GL_UNSIGNED_BYTE,
        data
    );
    glGenerateMipmap(GL_TEXTURE_2D);
    
    // assumes the texture is already currently bound to GL_TEXTURE_2D
    // s,t,r is the equivalent to x,y,z of texture axes
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    stbi_image_free(data);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    return true;
}

