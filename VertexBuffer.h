#ifndef VERTEX_BUFFER_H_INCLUDED
#define VERTEX_BUFFER_H_INCLUDED

#include <iostream>
#include <vector>

#include <GL/glew.h>

class VertexBuffer
{

friend class Renderer;
    
public:
    VertexBuffer();
    ~VertexBuffer();
    
    // TODO - handle other vertex attributes
    void Init(std::vector<float>& vertices, int floatsPerVertex = 3);
    void Init(std::vector<float>& vertices, std::vector<unsigned int>& indices, int floatsPerVertex = 3);
    // TODO - handle other vertex attributes
    void Init(float* vertices, size_t verticesSize, std::vector<unsigned int>& indices, int floatsPerVertex = 3);
    
    void UpdateData(std::vector<float>& vertices);
    
private:
    GLuint mVAO;
    GLuint mVBO;
    GLuint mEBO;
    
    unsigned long int mNumFloats;
    unsigned long int mNumVertices;
    unsigned long int mNumIndices;
};

#endif // VERTEX_BUFFER_H_INCLUDED
