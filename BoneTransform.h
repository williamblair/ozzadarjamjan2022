#ifndef BONE_TRANSFORM_H_INCLUDED
#define BONE_TRANSFORM_H_INCLUDED

#include <Math.h>

struct BoneTransform
{
    Quat rotation;
    Vec3 translation;
    
    Mat4 ToMatrix() const;
    
    static BoneTransform Interpolate(
        const BoneTransform& a, 
        const BoneTransform& b, 
        float f
    );
};

#endif // BONE_TRANSFORM_H_INCLUDED
