#include <MoveComponent.h>

MoveComponent::MoveComponent() :
    position(Vec3(0.0f, 0.0f, 0.0f)),
    target(Vec3(0.0f, 0.0f, -1.0f)),
    up(Vec3(0.0f, 1.0f, 0.0f)),
    forward(Vec3(0.0f, 0.0f, -1.0f)),
    right(Vec3(1.0f, 0.0f, 0.0f)),
    pitch(0.0f),
    yaw(0.0f)
{}

void MoveComponent::Update()
{
    // calculate right axis
    right = Math::angleAxis(Math::ToRadians(yaw), Vec3(0.0f, 1.0f, 0.0f)) * // yaw about y axis
        Vec3(1.0f, 0.0f, 0.0f);
    right = Math::Normalize(right);
    
    // calculate up axis
    up = Math::angleAxis(Math::ToRadians(pitch), right) * // pitch about rotated x axis
        Vec3(0.0f, 1.0f, 0.0f);
    up = Math::Normalize(up);
    
    // calculate forward axis
    forward = Math::Cross(up, right);
    forward = Math::Normalize(forward);
    
    // set target equal to a bit forward from the eye position
    target = position + forward;
}
