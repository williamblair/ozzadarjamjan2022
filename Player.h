#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include <algorithm>

#include <Math.h>
#include <AABB.h>
#include <MoveComponent.h>
#include <Camera.h>
#include <Input.h>
#include <SoundEffect.h>
#include <TileMap.h>

class Player
{
public:
    
    Player();
    ~Player();
    
    // size of the player's AABB
    void SetSize(float size) {
        float halfSize = size / 2.0f;
        mSize = size;
        mAabb.max = Math::Vec3FromVals(halfSize, halfSize, halfSize);
        mAabb.min = Math::Vec3FromVals(-halfSize, -halfSize, -halfSize);
    }
    void SetWalkEffect(SoundEffect* e) { mWalkEffect = e; }
    
    void SetPosition(const Vec3& pos) {
        mPosition = pos;
        mCamera.GetMoveComponent().position = pos;
        mCamera.GetMoveComponent().position.y += mSize;
    }
    Vec3& GetPosition() { return mPosition; }
    void SetYaw(const float yaw) {
        mYaw = yaw;
        mCamera.GetMoveComponent().yaw = yaw;
    }
    
    void AddCollider(AABB& aabb) {
        mColliders.push_back(&aabb);
    }
    void AddColliders(std::vector<AABB>& aabbs) {
        for (auto& aabb : aabbs) {
            mColliders.push_back(&aabb);
        }
    }
    void RemoveCollider(AABB& aabb) {
        auto pos = std::find(
            mColliders.begin(),
            mColliders.end(),
            &aabb
        );
        if (pos != mColliders.end()) {
            mColliders.erase(pos);
        }
    }
    
    Mat4& GetViewMat() { return mViewMat; }
    AABB& GetAabb() { return mAabb; }
    
    void Move(Input& input, const float dt);
    void Update();

    void StopWalkEffect() {
        if (mPlayingWalkEffect) {
            mWalkEffect->Stop(sWalkEffectChannel);
            mPlayingWalkEffect = false;
        }
    }
    
private:
    Vec3 mPosition;
    Mat4 mViewMat;
    float mYaw;
    float mSize;
    AABB mAabb;
    std::vector<AABB*> mColliders;
    FPSCamera mCamera;
    float mWalkingUpTime;
    SoundEffect* mWalkEffect;
    bool mPlayingWalkEffect;
    bool mWalkEffectStopped;

    static const int sWalkEffectChannel = 0;
    
    bool PlayerCollidesAtCurPos() {
        // TODO - spatial map or something so we don't have to check
        // every AABB
        for (AABB* aabb : mColliders)
        {
            if (aabb->Intersects(mAabb)) {
                return true;
            }
        }
        return false;
    }
};

#endif // PLAYER_H_INCLUDED
