#include <Sprite.h>

bool Sprite::SpriteShaderLoaded = false;
Shader Sprite::SpriteShader;

Sprite::Sprite() :
    mUv(0.0f, 0.0f),
    mWh(0.0f, 0.0f),
    mSpriteSize(0.0f, 0.0f),
    mScreenSize(0.0f, 0.0f),
    mRotation(0.0f),
    mScale(1.0f, 1.0f, 1.0f)
{
}

Sprite::~Sprite()
{
}

void Sprite::Load(const std::string& fileName, int screenW, int screenH)
{
    if (!mTexture.Load(fileName)) {
        throw std::runtime_error("Failed to load texture");
    }
    mSpriteSize.x = mTexture.GetWidth();
    mSpriteSize.y = mTexture.GetHeight();
    mWh.x = mSpriteSize.x;
    mWh.y = mSpriteSize.y;
    mScreenSize.x = screenW;
    mScreenSize.y = screenH;
    
    if (!SpriteShaderLoaded) {
        SpriteShader.Init(
            "Shaders/SpriteVert.glsl", "Shaders/SpriteFrag.glsl"
        );
        SpriteShaderLoaded = true;
    }
    
    // will be overwritten via UpdateVertBuf anyways
    std::vector<float> verts = {
        // pos              uv
        -1.0f, -1.0f,     0.0f, 0.0f,     // bottom left
        1.0f, -1.0f,    1.0f, 0.0f,     // bottom right
        1.0f, 1.0f,       1.0f, 1.0f,     // top right
        
        -1.0f, -1.0f,     0.0f, 0.0f,     // bottom left
        1.0f, 1.0f,       1.0f, 1.0f,     // top right
        -1.0f, 1.0f,        0.0f, 1.0f      // top left
    };
    
    // 4 floats per vertex
    mVertBuf.Init(verts, 4);
    UpdateVertBuf();
}

void Sprite::Draw(Renderer& render, int x, int y)
{
    glDisable(GL_DEPTH_TEST);
    
    float ndcX = pixWidthToNDCCoord(x);
    float ndcY = -pixHeightToNDCCoord(y);
    //Vec2 translate(ndcX, ndcY);
    Quat rotQuat = Math::angleAxis(
        Math::ToRadians(mRotation), 
        Vec3(0.0f,0.0f,1.0f)
    );
    
    Mat4 transform =
        
        Math::translate(ndcX, ndcY, 0.0f) *

        (Math::translate(-1.0f, 1.0f, 0.0f) * // move back
        Math::QuatToMat4(rotQuat) * // rotate
        Math::translate(1.0f, -1.0f, 0.0f)) * // move to center before rotation;

        (Math::translate(-1.0f, 1.0f, 0.0f) * // move back
        Math::scale(mScale) * // scale
        Math::translate(1.0f, -1.0f, 0.0f)); // move to center before scale
        
    SpriteShader.Use();
    //SpriteShader.SetVec2("uTranslate", translate);
    SpriteShader.SetMat4("uTransform", transform);
    
    render.SetTexture(mTexture);
    SpriteShader.SetInt("uTexture", 0); // GL_TEXTURE0
    
    render.DrawVertexBuffer(mVertBuf);
    
    glEnable(GL_DEPTH_TEST);
}
